" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0
"
" Modifications to the slate theme

hi clear
if exists("syntax_on")
  syntax reset
endif

"Load the 'base' colorscheme - the one to alter
runtime colors/slate.vim
":colorscheme slate

let colors_name = "slate_svb"

:hi Comment term=bold ctermfg=242 guifg=grey40
:"hi String term=bold guifg=SkyBlue ctermfg=cyan
:"hi String term=bold guifg=SkyBlue ctermfg=106
:hi Identifier guifg=salmon ctermfg=114
:hi Include guifg=red ctermfg=114
:hi PreProc guifg=red guibg=white ctermfg=114
:hi Operator guifg=Red ctermfg=114
:hi search guibg=peru guifg=wheat cterm=none ctermfg=black ctermbg=yellow
:hi clear SpellBad
:hi clear SpellCap
:hi clear SpellLocal
:"hi clear SpellRare
:hi SpellBad cterm=underline ctermfg=196
:hi SpellLocal ctermfg=237 ctermbg=253
:"hi SpellRare ctermfg=243 ctermbg=255
:"hi SpellErrors guifg=White guibg=Red cterm=underline ctermfg=7
:"hi LineNr guifg=grey50 ctermfg=114
:hi LineNr guifg=grey50 ctermfg=242
:"hi Function guifg=navajowhite ctermfg=114
:"hi Type guifg=CornflowerBlue ctermfg=2
:hi Constant guifg=#ffa0a0 ctermfg=28 "brown is standard
:"hi Special guifg=darkkhaki ctermfg=131
:hi Special guifg=darkkhaki ctermfg=101
:hi Visual  cterm=reverse ctermbg=black

"---------------------------------------------------------------------
" HTML (and markdown)
"---------------------------------------------------------------------

:hi htmlH1 guifg=gold gui=bold cterm=bold ctermfg=yellow
:hi htmlH2 guifg=gold gui=bold cterm=bold ctermfg=yellow
:hi htmlH3 guifg=gold gui=bold cterm=bold ctermfg=yellow
:hi htmlH4 guifg=gold gui=bold cterm=bold ctermfg=yellow
:hi htmlH5 guifg=gold gui=bold cterm=bold ctermfg=yellow

":hi htmlTag       ctermfg=217
:hi htmlComment       ctermfg=245
:hi htmlCommentPart   ctermfg=245
:hi LineNr guifg=grey50 ctermfg=242
":hi Include guifg=red ctermfg=114
:hi htmlBold term=bold cterm=bold gui=bold
:hi htmlItalic term=italic ctermbg=238 ctermfg=255 gui=bold

"---------------------------------------------------------------------
" HTML
"---------------------------------------------------------------------
"add words to htmlArg
"syn keyword htmlArg datatype resource property
":hi htmlArg guifg=red guibg=white ctermfg=42
":hi htmlStatement guifg=CornflowerBlue ctermfg=121 "palegreen
":hi htmlLink ctermfg=67 cterm=underline
":hi htmlString ctermfg=108

:hi htmlLink ctermfg=75 cterm=underline
" default: term=italic cterm=italic gui=italic
:hi Special term=bold cterm=bold gui=bold

