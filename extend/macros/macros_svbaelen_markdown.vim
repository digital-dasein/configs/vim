" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0

function SetMarkdownSurroundMappings()
  " Surround (plugin needs to be loaded!)
  "let b:surround_45 = "```\n\r\n```"
  let b:surround_{char2nr('b')} = "**\r**"
  let b:surround_{char2nr('i')} = "*\r*"
  "let b:surround_{char2nr('a')} = "[\r](<++>)"
  vmap Sa S]v$:s/]/](<++>)/ <CR><C-j>

  "vmap Sa S<a href="">v$:s/href=""/href="<++>"/ <CR><C-j>
  "vmap Sb S<b>
  "vmap Si S<i>
  "vmap Ss S<samp>
  "vmap Sl S<li>
  "" wrap every line of selection with <samp></samp>
  "vmap SS :normal yss<samp><CR>
  "" wrap every line of selection with <li></li>
  "vmap SL :normal yss<li><CR>
endfunction
