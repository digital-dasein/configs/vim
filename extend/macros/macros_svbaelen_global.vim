" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0
"
" Global macros for Vim

" Header for README's and instructions (ASCII-files)
nnoremap <leader><S-H> <Esc>i---------------------------------------------------------------------<CR>Author(s):    Senne Van Baelen<CR>Contact:      senne.vanbaelen@kuleuven.be<CR>Date created: <C-R>=strftime("%Y-%m-%d")<CR><CR>Keywords:     IMP research group, kuleuven, <CR>Links:        <http://link1><CR>              <https://link2><CR><Esc>i---------------------------------------------------------------------<CR>Short:        .<CR>---------------------------------------------------------------------<CR><Esc>kkkkkkllllllllllllllllllllllllllllllllllllllllllll

" With last updated field
" nnoremap <leader><S-H> <Esc>i---------------------------------------------------------------------<CR>Author: Senne Van Baelen<CR>Contact:      senne.vanbaelen@kuleuven.be<CR>Date created: <C-R>=strftime("%Y-%m-%d")<CR><CR>Last update:  <C-R>=strftime("%d-%m-%Y")<CR><CR>Keywords:     imp research group, kuleuven, <CR>Links:        <http://link1><CR>              <https://link2><CR><Esc>i---------------------------------------------------------------------<CR>Short:        .<CR>---------------------------------------------------------------------<CR><Esc>kkkkkklllllllllllll

" New note with date
nnoremap <leader>n <Esc>i# [, <C-R>=strftime("%Y/%m/%d")<CR>]<esc>hhhhhhhhhhhhi
