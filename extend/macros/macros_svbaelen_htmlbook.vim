" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0

" Custom macros related to writing HTML book
"
"---------------------------------------------------------------------
" Config
"---------------------------------------------------------------------

" by default, do not set anything here (rather in autocmd)
let s:set_mappings = 0      " define mappings in here (1, or elsewhere: 0)

" overwrite by global
if g:set_g_html
  let s:set_mappings = 1
endif

"---------------------------------------------------------------------
" Functions (wrappers)
"---------------------------------------------------------------------

" macro directory
function! RunConcatHtml()
  " this function prints all info messages, and put the bibtex warnings in
  " an alert
  ":!./utils/concat_html.sh -r -ro "-lf -http -bib -s ieeetr"
  :!./utils/concat_html.sh -i "./public/html" -m "public/_main.html" -o "public" -r -ro "-lf -http -bib -s ieeetr"
  :!./utils/refresh_firefox_tab.sh --tab-nr 1
endfunction

function! RunConcatHtmlSwitch()
  " this functions runs quietly (no warnings in alert)
  :!./utils/concat_html.sh -i "./public/html" -m "public/_main.html" -o "public"  -r -q -ro "-q -lf -http -bib -s ieeetr"
  :!./utils/refresh_firefox_tab.sh --switch-window --tab-nr 1
endfunction

" only wrapper functions here, mappings are defined elsewhere

function! NewHtmlSection()
  let id = input('Enter section-id (h2):', '')
  put='<section id=\"' . id . '\" rel=\"schema:hasPart\" resource=\"#'. id .'\">'
  put='  <h2 property=\"schema:name\"><++></h2>'
  put='  <div datatype=\"rdf:HTML\"  property=\"schema:description\" resource=\"#<++>\" typeof=\"<++>\">'
  put='    <++>'
  put='  </div><!--end div-h2-section-->'
  put='</section><!--end h2-section (' .id. ')-->'
  "replace whitespace before id (why does this happen?)
  ":%s/id=" /id="/
  ":%s/resource="# /resource="#/
  ":%s/section ( /section (/
  :normal kkkkkv0ojjjjj$=
  :call IMAP_Jumpfunc('', 0)
endfunction

function! NewHtmlSubSection()
  let id = input('Enter subsection-id (h3):', '')
  put='<section id=\"' . id . '\" rel=\"schema:hasPart\" resource=\"#'. id .'\">'
  put='  <h3 property=\"schema:name\"><++></h3>'
  put='  <div datatype=\"rdf:HTML\"  property=\"schema:description\" resource=\"#<++>\" typeof=\"<++>\">'
  put='    <++>'
  put='  </div><!--end div-h3-section-->'
  put='</section><!--end h3-section (' .id. ')-->'
  "replace whitespace before id (why does this happen?)
  ":%s/id=" /id="/
  ":%s/resource="# /resource="#/
  ":%s/section ( /section (/
  :normal kkkkkv0ojjjjj$=
  :call IMAP_Jumpfunc('', 0)
endfunction

function! NewHtmlSubSubSection()
  let id = input('Enter subsubsection-id (h4):', '')
  put='<section id=\"' . id . '\" rel=\"schema:hasPart\" resource=\"#'. id .'\">'
  put='  <h4 property=\"schema:name\"><++></h4>'
  put='  <div datatype=\"rdf:HTML\"  property=\"schema:description\" resource=\"#<++>\" typeof=\"<++>\">'
  put='    <++>'
  put='  </div><!--end div-h4-section-->'
  put='</section><!--end h4-section (' .id. ')-->'
  "replace whitespace before id (why does this happen?)
  ":%s/id=" /id="/
  ":%s/resource="# /resource="#/
  ":%s/section ( /section (/
  :normal kkkkkv0ojjjjj$=
  :call IMAP_Jumpfunc('', 0)
endfunction

function! NewHtmlSubSubSubSection()
  let id = input('Enter subsubsubsection-id (h5):', '')
  put='<section id=\"' . id . '\" rel=\"schema:hasPart\" resource=\"#'. id .'\">'
  put='  <h5 property=\"schema:name\"><++></h5>'
  put='  <div datatype=\"rdf:HTML\"  property=\"schema:description\" resource=\"#<++>\" typeof=\"<++>\">'
  put='    <++>'
  put='  </div><!--end div-h5-section-->'
  put='</section><!--end h5-section (' .id. ')-->'
  "replace whitespace before id (why does this happen?)
  ":%s/id=" /id="/
  ":%s/resource="# /resource="#/
  ":%s/section ( /section (/
  :normal kkkkkv0ojjjjj$=
  :call IMAP_Jumpfunc('', 0)
endfunction

function! CreateSection()
  let hnr = input('h-nr:')
  if hnr  == 2
    :call NewHtmlSection()
  elseif hnr == 3
    :call NewHtmlSubSection()
  elseif hnr == 4
    :call NewHtmlSubSubSection()
  elseif hnr == 5
    :call NewHtmlSubSubSubSection()
  endif
endfunction

function! SetHtmlBookMappings()
  "nnoremap <leader>n <Esc>i<!--================ SLIDE [[[ ================--><CR><section><CR><++><CR></section><CR><!--]]]--> <Esc>kkkkwwhi
  nnoremap <leader>n <Esc>i <!-- SLIDE  [[[ --><CR><section class="slide"><CR><++><CR><my-footer></my-footer><CR></section><CR><!--]]]--> <Esc>kkkkkv0ojjjjj$=<Esc>kwwwhi
  nnoremap <leader>g1 <Esc>i <div class="gr-1" style="grid-template-columns: <++>%"><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR></div><!-- end grid --><Esc>kkkkv0ojjjj$=
  nnoremap <leader>g2  <Esc>i <div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR></div><!-- end grid --><Esc>kkkkkkkkv0ojjjjjjjj$=
  nnoremap <leader>g3 <Esc>i <div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR></div><!-- end grid --><Esc>kkkkkkkkkkkkv0ojjjjjjjjjjjj$=
  nnoremap <leader>ic  <Esc>i <code class="ic"><++></code><Esc>v0o$=
  nnoremap <leader>ib  <Esc>i <code class="ic"><++></code><Esc>v0o$=
  nnoremap <leader>ct  <Esc>i <pre><code class="text"><++></code></pre><Esc>v0o$=
  nnoremap <leader>c1  <Esc>i <figure id="<++>" class="cb-singleline ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR></figure><!--end listing--><Esc>kkv0ojjj$=
  nnoremap <leader>cl  <Esc>i <figure id="<++>" class="cb-singleline ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR></figure><!--end listing--><Esc>kkv0ojjj$=
  nnoremap <leader>cb  <Esc>i <figure id="<++>" class="listing ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR><div class="lcap"><++></div><CR></figure><!--end listing--><Esc>kkkv0ojjj$=
  nnoremap <leader>lb  <Esc>i <figure id="<++>" class="listing ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR><div class="lcap"><++></div><CR></figure><!--end listing--><Esc>kkkv0ojjj$=
  nnoremap <leader>l  <Esc>i <figure id="<++>" class="listing ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR><div class="lcap"><++></div><CR></figure><!--end listing--><Esc>kkkv0ojjj$=
  nnoremap <leader>f <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>ff <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>i <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>ii <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>fi <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>im <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=|
  nnoremap <leader>f2 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkv0ojjjjjjjjjjjj$=
  nnoremap <leader>i2 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkv0ojjjjjjjjjjjj$=
  nnoremap <leader>f3 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkkkkkv0ojjjjjjjjjjjjjjjj$=
  nnoremap <leader>i3 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkkkkkv0ojjjjjjjjjjjjjjjj$=
  nnoremap <S-s> :call CreateSection()<CR>
  nnoremap <leader>s :call CreateSection()<CR>
  nnoremap <leader>e <Esc>i <figure id="<++>" class="equation" typeof="doco:FormulaBox"><CR>$$<CR><++><CR>$$<CR></figure><Esc>kkkkv0ojjjj$=
  " nnoremap <leader>s1 :call NewHtmlSection()<CR>
  " nnoremap <leader>s2 :call NewHtmlSection()<CR>
  " nnoremap <leader>s3 :call NewHtmlSubSection()<CR>
  " nnoremap <leader>s4 :call NewHtmlSubSubSection()<CR>
  " nnoremap <leader>s5 :call NewHtmlSubSubSubSection()<CR>
  nnoremap <leader>qb <Esc>i <blockquote class="quoteblock"><CR><++><CR></blockquote><Esc>kkv0ojj$=
  nnoremap <leader>bq <Esc>i <blockquote class="quoteblock"><CR><++><CR></blockquote><Esc>kkv0ojj$=
  normal zM
  nnoremap <leader>ch :call RunConcatHtml()<CR>
  nnoremap <leader>R :call RunConcatHtml()<CR>
  nnoremap <leader>r :call RunConcatHtmlSwitch()<CR><CR>
  nnoremap <S-r> :call RunConcatHtmlSwitch()<CR><CR>
endfunction

"---------------------------------------------------------------------
" Apply (globally)
"---------------------------------------------------------------------

if s:set_mappings
  call SetHtmlBookMappins()
endif
