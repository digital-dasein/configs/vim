" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0

" Source wrapper functions and/or define mappings globally

let g:macrosdir = g:extdir . "/macros"

exec "source " . g:macrosdir . "/macros_svbaelen_global.vim"
exec "source " . g:macrosdir . "/macros_svbaelen_html.vim"
exec "source " . g:macrosdir . "/macros_svbaelen_htmlbook.vim"
exec "source " . g:macrosdir . "/macros_svbaelen_markdown.vim"
