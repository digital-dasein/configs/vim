" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0

" VIM plugin extensions
" configured in ./vimext_svbaelen_plugins_config.vim

Plug 'nvie/vim-flake8'
Plug 'hdima/python-syntax'
Plug 'marcweber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'wojtekmach/vim-rename'
Plug 'gerw/vim-latex-suite'
Plug 'w0rp/ale'
Plug 'godlygeek/tabular'
Plug 'pangloss/vim-javascript'
Plug 'tpope/vim-surround'
"Plug 'plasticboy/vim-markdown'
Plug 'preservim/vim-markdown'
Plug 'leafgarland/typescript-vim'
"Plug 'SirVer/ultisnips'                 " not really using this one...
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'
Plug 'tmhedberg/simpylfold'
Plug 'junegunn/fzf'
"Plug 'junegunn/fzf.vim', {'commit': '4145f53f3d343c389ff974b1f1a68eeb39fba18b'}
Plug 'junegunn/fzf.vim'
Plug 'jonsmithers/vim-html-template-literals'
Plug 'pangloss/vim-javascript'
Plug 'prettier/vim-prettier', {
      \ 'do': 'yarn install --frozen-lockfile --production',
      \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json',
      \         'graphql', 'markdown', 'vue', 'svelte', 'yaml', 'html'] },
Plug 'lepture/vim-jinja'
"https://github.com/ycm-core/YouCompleteMe#linux-64-bit
" YouCompleteMe is too verbose / distracting for my taste
"Plug 'ycm-core/YouCompleteMe'
" Airline overruled with lightline (default)
"Plug 'vim-airline/vim-airline'
"Plug 'vim-airline/vim-airline-themes'
