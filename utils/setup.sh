#!/bin/sh

# SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
# SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
# SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
#
# SPDX-License-Identifier: CC0-1.0

# config
MAIN_VIMRC_REPO="./vimrc"
VIM_DIR="$HOME/.vim"
SWAPFILE_DIR="$VIM_DIR/swapfiles"
VIMRC_PATH_OUT="$HOME/.vimrc"
OVERWRITE=0
UPDATE_VIMRC=1
UPDATE_VIM_DIR=0
ALL_EXTENSIONS=0
CUSTOM_SNIPPETS=1
SILENT=0
OLD_VERSIONS=0

EXEC_PATH=$0
PROJ_DIR="$(dirname "${EXEC_PATH}")/.."

usage()
{
  # In the here-doc (inside EOF - EOF):
  USAGE=$(
  cat << EOF | sed 's/\t\+\t/\t\t\t    /g'
Usage: $0 [-d VIM_DIR ] [-ov VIMRC_PATH] [-f] [-s] [-a] [-o] [-nu] [-h]

Configure extensions in the main './vimrc' file (~L42)

Options:
    -d|--vim-dir DIR        vim target directory (default $VIM_DIR'; if
                            non-default, this also sets 'g:vimdir' in vimrc)
    -ov|--output-vrc FILE   vimrc target filepath (default $VIMRC_PATH_OUT)
    -f|--force-overwrite    force overwrite in vim directory
    -a|--all-extensions     load all extensions (default: no extensinos)
    -s|--silent             do not print INFO messages
    -ns|--no-snippets       do not copy custom snippets
    -o|--old-versions       use config files compatible with vim version > 6
    -nu|--no-update         avoid updating vimrc based on filepath preferences
                            default: update when vim dir != \$HOME/.vim
    -h|--help|--usage       show help
EOF
)
printf "${USAGE}\n"
}

# options and positional arugments
while [ -n "$1" ]; do
  case "$1" in
    -h|--help|--usage)
      usage;
      exit 1
      ;;
    -f|--force-overwrite)
      OVERWRITE=1
      ;;
    -nu|--no-update)
      UPDATE_VIMRC=0
      ;;
    -a|--all-extensions)
      ALL_EXTENSIONS=1
      ;;
    -s|--silent)
      SILENT=1
      ;;
    -ns|--no-snippets)
      CUSTOM_SNIPPETS=0
      ;;
    -o|--old-version)
      OLD_VERSIONS=1
      ;;
    -d|--vim-dir) VIM_DIR=$2
      shift
      ;;
    -ov|--output-vrc) VIMRC_PATH_OUT=$2
      shift
      ;;
    -*|--*)
      printf "[ERROR] option not recognized.  Exiting...\n" usage;
      exit 1
      ;;
    *)
      # Default case: this is requred to obtain a positional arugment at $1
      # later (if desired)
      break
  esac
  shift
done

# create output dir if not exist
if [ ! -d "${VIM_DIR}" ]; then
    if [ $SILENT = 0 ];then
        printf "[INFO] Directory '${VIM_DIR}' does not exist. Creating...\n"
    fi
    mkdir -p "$VIM_DIR"
fi

VIMRC_DIR="$(dirname "${VIMRC_PATH_OUT}")"

if [ ! -d "${VIMRC_DIR}" ]; then
    printf "[ERROR] Directory '${VIMRC_DIR}' (for vimrc) does not exist.\n"
    printf "Exiting...\n"
fi

# update when directories are non-default
if [ ${VIM_DIR} != "$HOME/.vim" ];then
    UPDATE_VIM_DIR=1
fi

if [ ! -d "${VIM_DIR}/swapfiles" ]; then
    if [ $SILENT = 0 ];then
        printf "[INFO] No swapfiles directory exist. Creating '${VIM_DIR}/swapfiles'\n"
    fi
    mkdir "${VIM_DIR}/swapfiles"
fi

# make sure you are in the project root path
if [ ! -d "$PROJ_DIR/extend"   ] ||
  [ ! -d "$PROJ_DIR/LICENSES" ] ||
  [ ! -e "$PROJ_DIR/README.md"  ]; then
  echo "[ERROR] '"$PROJ_DIR"' is not the main repo directory."
  echo "Navigate to repository, or profide an input directory [-i]."
  echo "Exiting!"
  exit 1;
fi

VIMRC_FILE="$PROJ_DIR/$MAIN_VIMRC_REPO"

# create tmp copy from this vimrc
cp $VIMRC_FILE  $VIMRC_FILE.tmp

if [ $UPDATE_VIMRC = 1 ];then
    if [ $UPDATE_VIM_DIR = 1 ];then
        sed -e  "/let g:vimdir/c\let g:vimdir = \"$VIM_DIR\"" \
            "$VIMRC_FILE.tmp" > "$VIMRC_FILE.new"
        mv -- "$VIMRC_FILE.new" "$VIMRC_FILE.tmp"
    fi
    if [ $ALL_EXTENSIONS = 1 ];then
        sed -e  "s/\"\\\ g:extdir/\\\ g:extdir/g" \
            "$VIMRC_FILE.tmp" > "$VIMRC_FILE.new"
        mv -- "$VIMRC_FILE.new" "$VIMRC_FILE.tmp"
        sed -e  "s/\"let g:extend_plugins_fpath/let g:extend_plugins_fpath/g" \
            "$VIMRC_FILE.tmp" > "$VIMRC_FILE.new"
        mv -- "$VIMRC_FILE.new" "$VIMRC_FILE.tmp"
        sed -e  "s/\" let g:extend_plugins_fpath/let g:extend_plugins_fpath/g" \
            "$VIMRC_FILE.tmp" > "$VIMRC_FILE.new"
        mv -- "$VIMRC_FILE.new" "$VIMRC_FILE.tmp"
    fi
    # remove all in-list and in-dict comments for compatibility with older vim
    # versions
    if [ $OLD_VERSIONS = 1 ];then
        sed -e  "/\"\\\/d"  "$VIMRC_FILE.tmp" > "$VIMRC_FILE.new"
        mv -- "$VIMRC_FILE.new" "$VIMRC_FILE.tmp"
        sed -e  "/\" \\\/d" "$VIMRC_FILE.tmp" > "$VIMRC_FILE.new"
        mv -- "$VIMRC_FILE.new" "$VIMRC_FILE.tmp"
    fi
fi

CP_OPTS="-ir"
if [ $OVERWRITE -eq 1 ];then
  CP_OPTS="-rf"
fi

# execute copy commands
cp  $CP_OPTS "$PROJ_DIR/extend" $VIM_DIR
# tmp remove snippets (possibly add later)
rm -rf $VIM_DIR/extend/snippets
if [ $SILENT -eq 0 ];then
    printf "[INFO] Copied folder '$PROJ_DIR/extend' to '$VIM_DIR'\n"
fi
cp  $CP_OPTS "$PROJ_DIR/colors" $VIM_DIR
if [ $SILENT -eq 0 ];then
    printf "[INFO] Copied folder '$PROJ_DIR/colors' to '$VIM_DIR'\n"
fi
cp $CP_OPTS "$VIMRC_FILE.tmp" "$VIMRC_PATH_OUT"
if [ $SILENT -eq 0 ];then
    printf "[INFO] Copied file '$VIMRC_FILE' to '$VIMRC_PATH_OUT'\n"
fi
rm -f "$VIMRC_FILE.tmp"

# snippets
if [ $CUSTOM_SNIPPETS -eq 1 ];then
    mkdir -p "$VIM_DIR/after"
    cp  $CP_OPTS "$PROJ_DIR/extend/snippets" $VIM_DIR/after
    if [ $SILENT = 0 ];then
        printf "[INFO] Copied folder '$PROJ_DIR/extend/snippets' to '$VIM_DIR/after'\n"
    fi
fi

if [ $SILENT = 0 ];then
    printf "[INFO] Configure your extensions in '$VIMRC_PATH_OUT'\n"
    printf "[INFO] Open a new file with vim to apply the new configs\n"
    printf "[INFO] Vim config finished.\n"
fi
