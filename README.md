<!--

 SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
 SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
 SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>

 SPDX-License-Identifier: CC0-1.0

-->

# Vim configs

`vim` is a [screen-based **text editor** program for 
Unix](https://en.wikipedia.org/wiki/Vim_(text_editor)). This repository 
contains a set of vim configurations, configurable to _your_ needs.

## Main vimrc

This main [`./vimrc`](./vimrc) file contains basic, _subjective_ optimisations 
that aim to **increase vim-productivity**.

While this "basic" config file (without extensions) already _does_ include 
several plugins, it should be noted that these are **lightweight** and **kept 
to a minimum** (install time &lt;&lt; 5 seconds). For a **_real_ minimalistic 
vim setup**, you should probably extend the default configs with just a couple 
of **_your_ favourite mappings/macros**.

## Setup and configure

### With utility

```sh
Usage: ./utils/setup.sh [-d VIM_DIR ] [-ov VIMRC_PATH] [-f] [-s] [-a] [-o] [-nu] [-h]

Configure extensions in the main './vimrc' file (~L42)

Options:
    -d|--vim-dir DIR        vim target directory (default '$HOME/.vim'; if
                            non-default, this also sets 'g:vimdir' in vimrc)
    -ov|--output-vrc FILE   vimrc target filepath (default '$HOME/.vimrc')
    -f|--force-overwrite    force overwrite in vim directory
    -a|--all-extensions     load all extensions (default: no extensinos)
    -s|--silent             do not print INFO messages
    -o|--old-versions       use config files compatible with vim version > 6
    -nu|--no-update         avoid updating vimrc based on filepath preferences
                            default: update when vim dir != $HOME/.vim
    -h|--help|--usage       show help

```

**NOTE**: when plugins are added in a later stage, i.e., _after_ the initial 
vim execution, don't forget to call `:PlugInstall` again to **enable them**.

#### Start fresh

If you&mdash;like most of us&mdash;start to loose track of your ramified 
configuration scripts, and you forgot what half of them even do, it could be a 
good idea to **start fresh**. For vim, this _could_ involve (some of) the 
following steps:

- `$ mv -t /some/backup/path  $HOME/.vim  $HOME/.vimrc`
- `$ cd /path/to/this/repository`
- `$ ./utils/setup.sh [OPTIONS]` (even without options, you are still in very 
  good hands!)

### Manually

Copy files to your vim folder or cherry pick configuration bits and add them to 
your existing configs. Alternatively, start a vim session like this:

```sh
$ vi(m) -u /path/to/vimrc
```

## Extensions to main config
<!-- $ tree {ext*,col**} -->

```sh
extend
├── macros
│   ├── macros_svbaelen_global.vim
│   ├── macros_svbaelen_htmlbook.vim
│   └── macros_svbaelen_html.vim
├── vimext_svbaelen_autocmd.vim
├── vimext_svbaelen_macros.vim
├── vimext_svbaelen_paths.vim
├── vimext_svbaelen_plugins_config.vim
├── vimext_svbaelen_plugins.vim
└── vimext_svbaelen_tmux.vim
colors
├── slate_svb_md.vim
└── slate_svb.vim
```
