<!--

 SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
 SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
 SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>

 SPDX-License-Identifier: CC0-1.0

-->

# Changelog

## Release v0.0.2

### Main Features

- Plugin simplification
- Minox fixes

### Commits

- 5870dfa29520a803863c368f5c0977255ff4e6f7 (**2022-02-25**) | remove simplyfold plugin (*svbaelen*)
- 317f3ecdb50df349ca0a11419da5551c38e2bc26 (**2022-02-24**) | Add custom markdown snippet, replace UltiSnips with SnipMate (*svbaelen*)
- a9e0452c7bed1725807a980b9ae56ab8bdae4776 (**2022-02-23**) | Update changelog (*svbaelen*)
- 3c41ede7b4343c87ecbb80961f9d503275ed706d (**2022-02-23**) | Fix undefined tmux variable (*svbaelen*)
- 49834b53a07ddb2cb30eae6ef7764bdafacb3ce0 (**2022-02-23**) | Add CHANGELOG (*svbaelen*)
- 7b67d47610a874d7ba104ee877a3933e89788d76 (**2022-02-22**) | Custom markdown folding (*svbaelen*)

## Release v0.0.1

### Main features

- Initial setup and decoupling of vim extensions
- Utility for automated setup

### Commits

- 7b67d47610a874d7ba104ee877a3933e89788d76 (**2022-02-22**) | Custom markdown folding (*svbaelen*)
- bceb2fa5fb775dadb3a6dbdfc3cc70385f4a42a5 (**2022-02-16**) | fix default old versinos (*svbaelen*)
- 53cff99e0faa4e355197b8d7804915f2b3877be8 (**2022-02-16**) | update opts in RM (*svbaelen*)
- 1f9859abd0fcfa617eba21a106f900e4d49caf61 (**2022-02-16**) | optional older version compatibilty (*svbaelen*)
- 84d670ecc425a58ac8a8fd168a54279127db2eb3 (**2022-02-16**) | fix compatibilty with older vim versions (*svbaelen*)
- 06c027e18aa5de575d14fd9116a1b175de94ad3d (**2022-02-16**) | minor adjustments to utility (*svbaelen*)
- 95fa27d4d0fcde7ed99622d7774e4f2e787b7a09 (**2022-02-16**) | test with parent dir (*svbaelen*)
- 92bf0cf65a8d425b15b816d86a4c8051abe41cbb (**2022-02-16**) | auto-detect proj dir (*svbaelen*)
- 9c1441c0b6bdaeea78223457872adce722722ccb (**2022-02-16**) | fix ov opt in utility (*svbaelen*)
- c606c6bc125717be2adaf869f7390476a3f0b6b9 (**2022-02-16**) | fzf mapping udpates and headers (*svbaelen*)
- 360eaeee1c79d1690b0cda3c3b9ecba9d07c1a66 (**2022-02-16**) | fix statusbar theme mods (*svbaelen*)
- 4f6e894508708ee9a138602cb7c999b5f23bd5e7 (**2022-02-16**) | README fixes (*svbaelen*)
- bf55bb14e3b2a818607ff336aeef7c9bedd10bfe (**2022-02-16**) | add extensions functionality to autosetup (*svbaelen*)
- 35ab74cc6ab3777916b57601af88105318d967df (**2022-02-15**) | minor adjustments (*svbaelen*)
- 7d412d4cb1d44d91cd50b82998973c407cfb2589 (**2022-02-15**) | updrm (*svbaelen*)
- d603d58e986df2dabd95ea99a24570c9a7145c33 (**2022-02-15**) | update README (*svbaelen*)
- af7d7a08a45d256efcc4bb608813254698e964fd (**2022-02-15**) | add licenses and conform to reuse (*svbaelen*)
- 2f4343e3ac1e2522db922ac45ea672e13a39c524 (**2022-02-15**) | fix netrw issues with dash (*svbaelen*)
- aa4a7179471026e88f4c445c8db0c27c2aa11df8 (**2022-02-15**) | getting there almost (*svbaelen*)
- 2b598f6d691c2ea1074508ee3b6ccbe2cdd06210 (**2022-02-15**) | getting there: (*svbaelen*)
- f3ef2fda90711b6f13c37167b5b47b9bd7c61998 (**2022-02-14**) | test with tempus totus (*svbaelen*)
- 557cacf60de10ac7e5e2d0d5270b91aff09592fb (**2022-02-14**) | first tests (*svbaelen*)

