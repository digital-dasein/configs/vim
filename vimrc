" ====================================================================
" VIM config (main)
" ====================================================================
"
" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0
"
" Date created: 2022-02-11
" Keywords:     vim, configs, text editor
" Links:        [repo] <https://gitlab.com/digital-dasein/configs/vim>
"               [vim-src]  <https://github.com/vim/vim>
"               [vim-install] <https://github.com/vim/vim#installation>
"
" Short:        Basic vim configuration file.
"
"               Use the ./utils/setup.sh script for an automated setup.
"               Configure and load extensions to this vimrc around line 42.
"
"               No extensions are loaded by default!
"
" Notes:        System-default configs (and thmees) for vim  can be found in:
"
"                   /usr/share/vim/
"
"               This vimrc already includes some useful plugins, using the
"               Vim-plug pluggin manager <https://github.com/junegunn/vim-plug>
"
"                   :PlugInstall  (automatically executes on first run)
"
"               Also note that extension plugins and their corresponding
"               configurations are two _separate_ files.
"
" ====================================================================

"---------------------------------------------------------------------
" Config
"---------------------------------------------------------------------

" general configs
let g:vimdir = "$HOME/.vim"                   " main vim directory
let g:plugindir = g:vimdir . "/plugged"       " plugin directory
let g:swapdir = g:vimdir . "/swapfiles//"     " swapfile directory

" configure extensions
let g:extdir = g:vimdir . "/extend"           " vimrc-extensions directory
" filepath containing additional plugins (uncomment or set to "none" to avoid)
"let g:extend_plugins_fpath = g:extdir . "/vimext_svbaelen_plugins.vim"

" uncomment / modify depending on your needs and preferences
let g:l_extension_files = [
      "\ g:extdir . "/vimext_svbaelen_plugins_config.vim",
      "\ g:extdir . "/vimext_svbaelen_paths.vim",
      "\ g:extdir . "/vimext_svbaelen_tmux.vim",
      "\ g:extdir . "/vimext_svbaelen_macros.vim",
      "\ g:extdir . "/vimext_svbaelen_autocmd.vim",
      \]

" additional configs
let g:set_g_html = 0          " set html mappings globally or
                              " elsewhere, e.g. in autocmd (recommended!)

"---------------------------------------------------------------------
" General
"---------------------------------------------------------------------

"map the leader key (3 options)
let mapleader = '\'
nmap , \
nmap <space> \

"Set lineNumbering:
set nu
"no linebreaking
set linebreak
set tw=79

set t_Co=256
set nocompatible
"filetype off
set encoding=utf-8

"to insert space characters whenever the tab key is pressed:
set expandtab

"set autoindent
syntax on

set tabstop=2
set softtabstop=2
set shiftwidth=2

"allow opening files form vim explores with "x"
let g:netrw_browsex_viewer= "xdg-open"
let g:netrw_localmovecmd="mv"
let g:netrw_localcopycmd="cp"

" hide pyc files
let g:netrw_list_hide= '*\.pyc$'
let g:netrw_list_hide= '*\.pyc'
let g:netrw_list_hide= '*.pyc'

" do not prompt all the time in netrw
let g:netrw_silent = 1

"check out and open recenly opened files
nmap <leader>o  :browse oldfiles<CR>

"copy to clipboard vi shell
set clipboard=unnamed
vmap <leader>c :w !pbcopy <CR><CR>
nmap <leader>C :%w !pbcopy <CR><CR>

"some quick and dirty copy-paste work and textwidth
nmap <leader>va ggvG$
vmap a g<Esc>ggvG
nmap <leader>gq gggqG
nmap <C-a> ggvG$
vmap <C-a> <Esc>ggvG$
imap <C-a> <Esc>ggvG$

" navigate between windows
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

" split navigations
set splitbelow
set splitright

nnoremap  <A-J> <C-W><C-J>
nnoremap <A-K> <C-W><C-K>
nnoremap <A-L> <C-W><C-L>
nnoremap <A-H> <C-W><C-H>
nnoremap  <A-Up> <C-W><C-J>
nnoremap <A-Down> <C-W><C-K>
nnoremap <A-Right> <C-W><C-L>
nnoremap <A-Left> <C-W><C-H>

" highligh
set hlsearch

" cancel highlight
inoremap <C-f> <Esc> :nohlsearch <CR><i>
nnoremap <C-f> :nohlsearch <CR>

" delete spacings on selected text
vnoremap <C-x>  :s/ //g  <CR> <ESC> :nohlsearch <CR>

" disable quote concealing in JSON files
let g:vim_json_conceal=0

" show command suggestinos (tab-able)
set wildmenu

" default colorscheme (built-in)
set background=dark
colorscheme ron

" highlight trailing whitespaces
highlight ExtraWhitespace ctermbg=242 guibg=red
match ExtraWhitespace /\s\+$/

"---------------------------------------------------------------------
" Buffers
"---------------------------------------------------------------------
"
" allow buffers to be hidden if you've modified a buffer.
set hidden

"nmap <leader>b :buffers<cr>
nmap <leader>b :set nomore <Bar> :ls <Bar> :set more <CR>:b<Space>

" delete buffer
nmap <leader>d :bd<CR>

" move to the next buffer
"nmap on :bnext<CR>
nmap <S-b> :bnext<CR>
nmap <Tab> :bnext<CR>

" move to the previous buffer
"nmap op :bprevious<CR>
nmap <S-p> :bprevious<CR>
nmap <S-Tab> :bprevious<CR>
nmap <leader>p :bprevious<CR>

" this replicates the idea of closing a tab
"nmap <leader>bq :bp <BAR> bd #<CR>
"nmap <leader>bc :bp <BAR> bd #<CR>

"---------------------------------------------------------------------
" Spelling
"---------------------------------------------------------------------

" set file for auto-downloading spell files
let g:spellfile_URL = 'http://ftp.vim.org/vim/runtime/spell'

function! Spelling()
  let spelling = input('Pick a spelling language: [0:None], [1:en-us], [2:en-gb], [3:nl], [4:en-gb + nl], [5:en-us + nl]: ')
  if spelling==0
    set nospell
  elseif spelling==1
    set spell spelllang=en_us
  elseif spelling==2
    set spell spelllang=en_gb
  elseif spelling==3
    set spell spelllang=nl
  elseif spelling==4
    set spell spelllang=nl,en_gb
  elseif spelling==5
    set spell spelllang=nl,en_us
  endif
  " Spelling not for URLs
  syn match UrlNoSpell '\w\+:\/\/[^[:space:]]\+' contains=@NoSpell
endfunction

"---------------------------------------------------------------------
" Functions
"---------------------------------------------------------------------

" this function returns true if the defined file or directory exists (and
" allows for variables)
" (from https://stackoverflow.com/a/23496813/1997354)
function PathExists(FileName)
  return !empty(glob(a:FileName))
endfunction

function BackgroundTransparant()
  hi Normal guibg=NONE ctermbg=NONE
endfunction

fun! TrimWhitespace()
  let l:save_cursor = getpos('.')
  %s/\s\+$//e
  call setpos('.', l:save_cursor)
endfun

function! RemoveTrimWhitespace()
  command! TrimWhitespace echo "No man, not in here"
endfunction

function! UpdateLightlineTheme(colorscheme)
    let g:lightline.colorscheme = a:colorscheme
    call lightline#init()
    call lightline#colorscheme()
    call lightline#update()
endfunction

function! ColorScheme()
  let scheme = input('Pick a ColorScheme: [1:Default], [2: papercolor], [3: slate], [4: slate_svb]: ')
  if scheme==1
    "Terminal or GUI
    set background=dark
    colorscheme ron
    call UpdateLightlineTheme("nord")
  elseif scheme==2
    set background=light
    colorscheme PaperColor
    call UpdateLightlineTheme("PaperColor")
  elseif scheme==3
    set background=dark
    colorscheme slate
    call UpdateLightlineTheme("nord")
  elseif scheme==4
    set background=dark
    colorscheme slate_svb
    call UpdateLightlineTheme("nord")
  endif
endfunction

"Or just call function after setup
command! ColorScheme call ColorScheme()
map <F5> :ColorScheme <cr>

function! ModifyLightlinePalette()
  let plugin_path = g:plugindir . "/lightline.vim"
  if PathExists(plugin_path)
    if g:lightline.colorscheme == "nord"
      let s:palette = g:lightline#colorscheme#{g:lightline.colorscheme}#palette
      """ inactive tabs
      let s:palette.tabline.left = [ ['#bcbcbc', '#666666', 255, 240]]
      """ active tab
      let s:palette.tabline.tabsel = [ ['#262626', '#bcbcbc', 233, 250] ]
      """ right tabline
      let s:palette.tabline.middle = [ ['#a8a8a8', '#666666', 248, 238] ]
      let s:palette.normal.left = [ [ "#E5E9F0", "#3B4252", 255, 240 ],
            \ ["#E5E9F0", "#3B4252", 7,  0] ]
      let s:palette.visual.left = [ [ '#000000', '#f2c68a', 232, 216 ] ]
      " not needed (error with init.vim; might be needed on different system):
      "call lightline#colorscheme()
    endif
  endif
endfunction

function LightlineToggle()
  let plugin_path = g:plugindir . "/lightline.vim"
    if &laststatus == 0
      set laststatus=2
      set showtabline=2
      if PathExists(plugin_path)
        call lightline#enable()
      endif
    else
      set laststatus=0
      set showtabline=0
      if PathExists(plugin_path)
        call lightline#disable()
      endif
    "call lightline#toggle()
  endif
endfunction

"---------------------------------------------------------------------
" Files and directories
"---------------------------------------------------------------------

"don't show swp files in explorer:
if PathExists(g:swapdir)
  execute "set directory^=" . g:swapdir
else
  echo "no swapfile directory set, so no directory can be set"
endif

"---------------------------------------------------------------------
" Load plugins
"---------------------------------------------------------------------

" basic plugins with a significant impact on productivity
" specify a directory for plugins
" - for Neovim: stdpath('data') . '/plugged'
" - avoid using standard Vim directory names like 'plugin'

" " automate installation
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

let g:plug_home = expand(g:plugindir)
call plug#begin()
" default plugins (all very lightweight, installed in << 5 seconds)
Plug 'itchyny/lightline.vim'
Plug 'itchyny/vim-gitbranch'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'scrooloose/nerdcommenter'
Plug 'NLKNguyen/papercolor-theme'       "light theme for sunny days
Plug 'ervandew/supertab'
Plug 'maximbaz/lightline-trailing-whitespace'
" additional plugings
let plugin_fpath = get(g:, 'extend_plugins_fpath', "none")
if  plugin_fpath != "none"
  exec "source " . plugin_fpath
endif

" initialize plugin system
call plug#end()

filetype plugin indent on    " required

"---------------------------------------------------------------------
" Lightline (Airline alternative)
"---------------------------------------------------------------------

let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'readonly', 'filename', 'modified' ] ],
      \   'right': [['lineinfo'],
      \             ['percent'],
      \             ['gitbranch', 'fileencoding'],
      \             ['trailing']]
      \ },
      \ 'tabline': {
      \   'left': [ ['buffers'] ],
      \   'right': [ [''] ]
      \ },
      \ 'component_expand': {
      \   'gitbranch': 'gitbranch#name',
      \   'buffers': 'lightline#bufferline#buffers',
      \   'trailing': 'lightline#trailing_whitespace#component'
      \ },
      \ 'component_type': {
      \   'buffers': 'tabsel',
      \   'trailing': 'error'
      \ }
      \ }

" set colorscheme like this to allow modification
let g:lightline.colorscheme = "nord"

set noshowmode
set laststatus=2
set showtabline=2

" customise colors
" <https://github.com/itchyny/lightline.vim/blob/master/autoload/lightline/colorscheme/nord.vim>

try
  call ModifyLightlinePalette()
catch
  if exists("g:loaded_lightline")
    " initial load
    call ModifyLightlinePalette()
  endif
endtry

" do not show whitespace error on specific filetypes
au bufEnter,BufNewFile,BufRead *.md,/tmp/mutt*
      \ let g:lightline.active.right = [ [ 'lineinfo' ],
                                       \ [ 'percent' ],
                                       \ ['gitbranch', 'fileencoding'] ]
au bufLeave *.md,/tmp/mutt
      \ let g:lightline.active.right = [ [ 'lineinfo' ],
                                       \ [ 'percent' ],
                                       \ ['gitbranch', 'fileencoding'],
                                       \ ['trailing'] ]

"---------------------------------------------------------------------
" Papercolor
"---------------------------------------------------------------------

" CUSTOMIZE COLORSCHEME (needs to be _before_ PaperColor command
" colors: <https://github.com/NLKNguyen/papercolor-theme/blob/d0d32dc5e92564b63ec905a9423984b5d503c24a/colors/PaperColor.vim>
let g:PaperColor_Theme_Options = {
      \   'theme': {
      \     'default.light': {
      \				'transparent_background': 0,
      \       'override' : {
      \         'color00' : ['#FFFFFF', '231'],
      \         'linenumber_fg' : ['#808080', '8'],
      \         'color07' : ['#000000', '232'],
      \         'color10' : ['#000000', '242 cterm=bold gui=bold']
      \       }
      \     }
      \   }
      \ }

"---------------------------------------------------------------------
" Nerdtree commenter
"---------------------------------------------------------------------


"---------------------------------------------------------------------
" Macros
"---------------------------------------------------------------------

" by default, only ASCII headers are included in this main config. Other macros
" can be found in the extensions. The reason for this include is to increase
" consistency in ASCII documents and code comments
" TODO: create are more generic function for this

" HEADER 1
nmap <leader>#
  \ <Esc>i#####################################################################<CR>
  \ <Esc>
nmap <leader>##
  \ <Esc>i#####################################################################<CR>
  \ <CR>#####################################################################<Esc>ki
nmap <leader>1
  \ <Esc>i#####################################################################<CR>
  \ <CR>#####################################################################<Esc>ki
" HEADER 2
nmap <leader>=
  \ <Esc>i=====================================================================<CR>
  \ <Esc>
nmap <leader>==
  \ <Esc>i=====================================================================<CR>
  \ <CR>=====================================================================<Esc>ki
nmap <leader>2
  \ <Esc>i=====================================================================<CR>
  \ <CR>=====================================================================<Esc>ki
" HEADER 3
" use underscore (_) here to avoid buffer issues in netrw with "-"
nmap <leader>_
  \ <Esc>i---------------------------------------------------------------------<CR>
  \ <Esc>
nmap <leader>__
  \ <Esc>i---------------------------------------------------------------------<CR>
  \ <CR>---------------------------------------------------------------------<Esc>ki
nmap <leader>3
  \ <Esc>i---------------------------------------------------------------------<CR>
  \ <CR>---------------------------------------------------------------------<Esc>ki

"---------------------------------------------------------------------
"Commands
"---------------------------------------------------------------------

command! TrimWhitespace call TrimWhitespace()
command! Spelling call Spelling()
command! BackgroundTransparant call BackgroundTransparant()
command! ColorScheme call ColorScheme()
command! LightlineToggle call LightlineToggle()

" map commands
nmap <leader>l :call LightlineToggle()<CR>

"---------------------------------------------------------------------
" Load extensions
"---------------------------------------------------------------------

"source $g:vimdir/extend/_vimext_load.vim
"exec "source " . g:vimdir . "/extend/_vimext_load.vim"
for extfile in g:l_extension_files
  exec "source " . extfile
endfor
