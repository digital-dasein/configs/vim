" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0
"
"#####################################################################
"# vimrc basic
"#####################################################################

"---------------------------------------------------------------------
" Author(s):    Senne Van Baelen
" Contact:      senne@digitaldasein.org
" Date created: 2019-12-01
" Keywords:     vimrc, basic
"---------------------------------------------------------------------
" Short:        Basic vimrc with vundle as plugin-manager. For plugin-less
"               version, checkout: ~/.vim/svbaelen/vimrc_minimal
"               this requires:
"               - Plugin 'vim-airline/vim-airline'
"               - Plugin 'tmhedberg/simpylfold'
"               - Plugin 'vim-airline/vim-airline-themes'
"               - Plugin 'scrooloose/nerdcommenter'
"               - Plugin 'NLKNguyen/papercolor-theme'

"---------------------------------------------------------------------

"map the leader key (3 options, see which one will be used most
let mapleader = '\'
nmap , \
nmap <space> \

"Set lineNumbering:
set nu
"no linebreaking
set linebreak
set tw=76

set t_Co=256
set nocompatible
filetype off

"To insert space characters whenever the tab key is pressed:
set expandtab

"set autoindent
"set lines=57 columns=88

"nmap <leader>W :set columns=88 set lines=57<CR>
syntax on

"extra write map
"command W write

set tabstop=2
set softtabstop=2
set shiftwidth=2

"Allow opening files form vim explores with "x"
let g:netrw_browsex_viewer= "xdg-open"
let g:netrw_localmovecmd="mv"
let g:netrw_localcopycmd="cp"

"set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

"MAP SAVING
noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>

"copy to clipboard vi shell
vmap <leader>c :w !pbcopy <CR><CR>
nmap <leader>C :%w !pbcopy <CR><CR>

"some quick and dirty copy-paste work and textwidth
nmap <leader>va ggvG$
vmap a g<Esc>ggvG
nmap <leader>gq gggqG
nmap <C-a> ggvG$
vmap <C-a> <Esc>ggvG$
imap <C-a> <Esc>ggvG$

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" Add all your plugins here (note older versions of Vundle used Bundle
" instead of Plugin)
" General:
Plugin 'vim-airline/vim-airline'
Plugin 'tmhedberg/simpylfold'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdcommenter'
Plugin 'NLKNguyen/papercolor-theme'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

"Navigate between windows
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

"Vim-Airline already has a function to show the buffers:
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" This allows buffers to be hidden if you've modified a buffer.
" This is almost a must if you wish to use buffers in this way.
set hidden

" To open a new empty buffer
nmap <leader>e :enew<cr>
nmap <leader>be :enew<cr>

" Delete buffer
nmap <leader>bd :bd <cr>

" Move to the next buffer
"nmap on :bnext<CR>
nmap <S-b> :bnext<CR>

" Move to the previous buffer
"nmap op :bprevious<CR>
nmap <S-p> :bprevious<CR>
nmap <leader>p :bprevious<CR>

" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>
nmap <leader>bc :bp <BAR> bd #<CR>

" Show all open buffers and their status (fzf plugin)
nmap <leader>bb :Buffer<CR>
nmap <leader>bl :Buffer<CR>

set splitbelow
set splitright

"split navigations
nnoremap  <A-J> <C-W><C-J>
nnoremap <A-K> <C-W><C-K>
nnoremap <A-L> <C-W><C-L>
nnoremap <A-H> <C-W><C-H>
nnoremap  <A-Up> <C-W><C-J>
nnoremap <A-Down> <C-W><C-K>
nnoremap <A-Right> <C-W><C-L>
nnoremap <A-Left> <C-W><C-H>

"cancel highlight
inoremap <C-f> <Esc> :nohlsearch <CR><i>
nnoremap <C-f> :nohlsearch <CR>

"Delete spacings
vnoremap <C-x>  :s/ //g  <CR> <ESC> :nohlsearch <CR>

"Replace spaces with hyphens (e.g. best practice in LaTeX) in current line
nnoremap <leader>s 0 v $ :s/\%V /-/g <CR>

" vim autocmd
autocmd BufNewFile,BufRead *
      \	if expand('%:t') !~ '\.'  |
      \ setlocal nospell |
      \ endif

" Hidden filen
autocmd BufNewFile,BufRead .*
      \ setlocal nospell |
      \ :syn match UrlNoSpell '\w\+:\/\/[^[:space:]]\+' contains=@NoSpell


au BufNewFile,BufRead *.py
      \ set tabstop=4 |
      \ set softtabstop=4 |
      \ set shiftwidth=4 |
      \ set textwidth=78 |
      \ set expandtab |
      \ set autoindent |
      \ set fileformat=unix |
      \ match Type /self/
"\ colorscheme gruvbox |
"\ set background=dark
"
au BufNewFile,BufRead *.c*
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2 |
      \ set textwidth=77 |
      \ set expandtab |
      \ set autoindent

au BufNewFile,BufRead *.html
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2 |
      \ set foldmethod=marker |
      \ set foldmarker=[[[,]]]

au BufNewFile,BufRead *.vim
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2

au BufNewFile,BufRead *.vimrc
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2

au BufNewFile,BufRead *.txt
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2

au BufNewFile,BufRead *.css
      \ set tabstop=4 |
      \ set softtabstop=4|
      \ set shiftwidth=4 |
      \ set foldmethod=marker |
      \ set foldmarker=[[[,]]]

au BufNewFile,BufRead *.sh
      \ set tabstop=4 |
      \ set softtabstop=4|
      \ set shiftwidth=4

" Note: for .tex files "\ref" is included in re RefGroup, however, href is
" not (/usr/share/vim/vim80/syntax/tex.vim) --> copy command for \ref to
" \href in autocmd (.tex)

au BufNewFile,BufRead *.tex
      \ set spell spelllang=en_us |
      \ set tabstop=2 |
      \ syn region texRefZone		matchgroup=texStatement start="\\v\=href{"
      \	end="}\|%stopzone\>"	contains=@texRefGroup |
      \ colorscheme slate |
      \ AirlineTheme luna |
      \ call GetModSlate() |
      \ set softtabstop=2|
      \ set shiftwidth=2

au BufNewFile,BufRead *.bib
      \ setlocal nospell

autocmd BufNewFile,BufRead /tmp/mutt*
      \ set spell spelllang=nl,en_us |
      \ set tw=72 |
      \ set nojs |
      \ set fo=watqc |
      \ hi clear SpellBad |
      \ set noautoindent |
      \ set comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:> |
      \ hi SpellBad cterm=underline ctermfg=red  |
      \ match ErrorMsg '\s\+$' |
      \ :ALEToggle |
      \ map q <Nop>

" not used for mutt (amymore)
"set noautoindent |
"set nosmartindent |

" Mark trailing spaces, so we know we are doing flowed format right
" \ match ErrorMsg '\s\+$' |

let g:NERDCustomDelimiters = { 'tex': { 'left': '%','right': '' } }

"NERDTree when starting vim. use :NERDTree for this
"--> https://github.com/jistr/vim-nerdtree-tabs
let g:nerdtree_tabs_open_on_gui_startup=0
let g:nerdtree_tabs_open_on_console_startup=0
"let g:nerdtree_tabs_open_on_new_tab=0

" Enable folding
set foldmethod=indent
set foldlevel=99

let g:SimpylFold_docstring_preview=1

"You still have to go to the end of the file
"otherwise <D-y> will have another (edit) function
nnoremap <D-y> :let g:ycm_auto_trigger=0<CR>                "turn off YCM
nnoremap <D-Y> :let g:ycm_auto_trigger=1<CR>                "turn on YCM

fun! TrimWhitespace()
  let l:save_cursor = getpos('.')
  %s/\s\+$//e
  call setpos('.', l:save_cursor)
endfun
command! TrimWhitespace call TrimWhitespace()

"hide/show status bar (or line)
set laststatus=0

"Toggle Status Bar bottom
nnoremap <leader>b :call ToggleHiddenAll()<CR>


"=========================================================
"Coloschemes
"=========================================================
"---------------------------------------------------------
" Customized colorschemes from plugins
"---------------------------------------------------------"

" CUSTOMIZE COLORSCHEME (needs to be _before_ PaperColor command
let g:PaperColor_Theme_Options = {
      \   'theme': {
      \     'default.light': {
      \				'transparent_background': 1,
      \       'override' : {
      \         'color00' : ['#FFFFFF', '231'],
      \         'linenumber_fg' : ['#808080', '8'],
      \         'linenumber_bg' : ['#FFFFFF', '230'],
      \         'color07' : ['#000000', '232']
      \       }
      \     }
      \   }
      \ }

"---------------------------------------------------------
" Set color schemes
"---------------------------------------------------------"
let g:airline_theme='luna'
set background=dark
colorscheme ron

function! ColorScheme()
  let scheme = input('Pick a ColorScheme: [1:Default],[2: papercolor]')
  if scheme==1
    "Terminal or GUI
    set background=dark
    colorscheme ron
    AirlineTheme luna
  elseif scheme==2
    set background=light
    colorscheme PaperColor
    AirlineTheme luna
  endif
endfunction

"Or just call function after setup
command! ColorScheme call ColorScheme()

map <F5> :ColorScheme <cr>

function! Spelling()
  let spelling = input('Pick a spelling language: [1:English], [2:Nederlands],[3:None]: ')
  if spelling==1
    set spell spelllang=en_us
  elseif spelling==2
    set spell spelllang=nl
  elseif spelling==3
    set nospell
  endif
  " Spelling not for URLs
  syn match UrlNoSpell '\w\+:\/\/[^[:space:]]\+' contains=@NoSpell
endfunction

command! Spelling call Spelling()
"set spell spelllang=en_gb

"Airline (bar)
let g:airline_mode_map = {
      \ '__' : '-----',
      \ 'n'  : '    NORMAL    ',
      \ 'i'  : '    INSERT    ',
      \ 'v'  : '    VISUAL    ',
      \ 'V'  : '    VISUAL    ',
      \ }

" /usr/share/vim/vim80/colors/
" Find variables you want to modify and check out the the default vim
" syntax, i.e. in /usr/share/vim/vim80/syntax/<language>.vim
"
" It is best to re-define some variables in your vimrc and leave the original
" files as-is.
"
" Check your current buffer syntax: ':setlocal syntax?'
"
"---------------------------------------------------------
" Redefine html syntax colors
"---------------------------------------------------------"
" default: term=italic cterm=italic gui=italic
highlight htmlItalic term=italic ctermbg=233 ctermfg=255 gui=bold
highlight htmlBold term=bold cterm=bold gui=bold


"########################################
"####### General macros for vim   #######
"########################################

" Normal mode
nnoremap <leader>pp <Esc> i print("debug_hello") <Esc>

" Section headers in ASCII-files
nnoremap <leader>- <Esc>i---------------------------------------------------------------------<CR><Esc>
nnoremap <leader>s- <Esc>i---------------------------------------------------------------------<CR><CR>---------------------------------------------------------------------<Esc>ki
nnoremap <leader># <Esc>i#####################################################################<CR><Esc>
nnoremap <leader>s# <Esc>i#####################################################################<CR><CR>#####################################################################<Esc>ki
nnoremap <leader>3 <Esc>i#####################################################################<CR><Esc>
nnoremap <leader>= <Esc>i=====================================================================<CR><Esc>
nnoremap <leader>s= <Esc>i=====================================================================<CR><CR>=====================================================================<Esc>ki

" Email signatures
nnoremap <leader>ss <Esc>iSenne Van Baelen<CR>--<CR>Department of Mechanical Engineering, KU Leuven<CR>Andreas Vesaliusstraat 13 - box 2600<CR>3000 Leuven<CR>Robotics Research Group<CR>https://www.mech.kuleuven.be/en/pma/research/robotics<Esc>i

" Header for README's and instructions (ASCII-files)
nnoremap <leader><S-H> <Esc>i---------------------------------------------------------------------<CR>Author(s):    Senne Van Baelen<CR>Contact:      senne.vanbaelen@kuleuven.be<CR>Date created: <C-R>=strftime("%Y-%m-%d")<CR><CR>Keywords:     IMP research group, kuleuven, <CR>Links:        <http://link1><CR>              <https://link2><CR><Esc>i---------------------------------------------------------------------<CR>Short:        .<CR>---------------------------------------------------------------------<CR><Esc>kkkkkkllllllllllllllllllllllllllllllllllllllllllll

" With last update
" nnoremap <leader><S-H> <Esc>i---------------------------------------------------------------------<CR>Author: Senne Van Baelen<CR>Contact:      senne.vanbaelen@kuleuven.be<CR>Date created: <C-R>=strftime("%Y-%m-%d")<CR><CR>Last update:  <C-R>=strftime("%d-%m-%Y")<CR><CR>Keywords:     imp research group, kuleuven, <CR>Links:        <http://link1><CR>              <https://link2><CR><Esc>i---------------------------------------------------------------------<CR>Short:        .<CR>---------------------------------------------------------------------<CR><Esc>kkkkkklllllllllllll

" New note in journal
nnoremap <leader>n <Esc>i# [, <C-R>=strftime("%Y/%m/%d")<CR>]<esc>hhhhhhhhhhhhi


"########################################
"#######   NERDTREE COMMENTER    ########
"########################################
let g:NERDCustomDelimiters = { 'c': { 'left': '//','right': '' } }
let g:NERDCustomDelimiters = { 'cpp': { 'left': '//','right': '' } }
let g:NERDCustomDelimiters = { 'moos': { 'left': '//','right': '' } }
let g:NERDCustomDelimiters = { 'h': { 'left': '//','right': '' } }
let g:NERDCustomDelimiters = { 'bhv': { 'left': '//','right': '' } }
