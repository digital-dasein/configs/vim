" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0
"
"########################################
"####### General macros for vim   #######
"########################################

" Normal mode
nnoremap <leader>pp <Esc> i print("debug_hello") <Esc>

" Section headers in ASCII-files -> moved to vim_ftype_autocmd
"nnoremap <leader>- <Esc>i---------------------------------------------------------------------<CR><Esc>
"nnoremap <leader>s- <Esc>i---------------------------------------------------------------------<CR><CR>---------------------------------------------------------------------<Esc>ki
"nnoremap <leader># <Esc>i#####################################################################<CR><Esc>
"nnoremap <leader>s# <Esc>i#####################################################################<CR><CR>#####################################################################<Esc>ki
"nnoremap <leader>3 <Esc>i#####################################################################<CR><Esc>
"nnoremap <leader>= <Esc>i=====================================================================<CR><Esc>
"nnoremap <leader>s= <Esc>i=====================================================================<CR><CR>=====================================================================<Esc>ki


" Header for README's and instructions (ASCII-files)
nnoremap <leader><S-H> <Esc>i---------------------------------------------------------------------<CR>Author(s):    Senne Van Baelen<CR>Contact:      senne.vanbaelen@kuleuven.be<CR>Date created: <C-R>=strftime("%Y-%m-%d")<CR><CR>Keywords:     IMP research group, kuleuven, <CR>Links:        <http://link1><CR>              <https://link2><CR><Esc>i---------------------------------------------------------------------<CR>Short:        .<CR>---------------------------------------------------------------------<CR><Esc>kkkkkkllllllllllllllllllllllllllllllllllllllllllll

" With last update
" nnoremap <leader><S-H> <Esc>i---------------------------------------------------------------------<CR>Author: Senne Van Baelen<CR>Contact:      senne.vanbaelen@kuleuven.be<CR>Date created: <C-R>=strftime("%Y-%m-%d")<CR><CR>Last update:  <C-R>=strftime("%d-%m-%Y")<CR><CR>Keywords:     imp research group, kuleuven, <CR>Links:        <http://link1><CR>              <https://link2><CR><Esc>i---------------------------------------------------------------------<CR>Short:        .<CR>---------------------------------------------------------------------<CR><Esc>kkkkkklllllllllllll

" Bill details (facturatiegegevens) KU Leuven
nnoremap <leader>fg <Esc>iFACTURATIEGEGEVENS:<CR>Mechanical Engineering Technology TC, Campus Group T Leuven<CR>Andreas Vesaliusstraat 13 - box 2600<CR>3000 Leuven<CR>VAT: BE 0419.052.173<Esc>i
" Bill details (facturatiegegevens) KU Leuven
nnoremap <leader>bb <Esc>iFACTURATIEGEGEVENS:<CR>Mechanical Engineering Technology TC, Campus Group T Leuven<CR>Andreas Vesaliusstraat 13 - box 2600<CR>3000 Leuven<CR>VAT: BE 0419.052.173<Esc>i


" New note in journal
nnoremap <leader>n <Esc>i# [, <C-R>=strftime("%Y/%m/%d")<CR>]<esc>hhhhhhhhhhhhi

" New minimal html file
nnoremap <leader>nh :call NewHtmlFileMinimal()<CR>|


function! NewHtmlFileMinimal()
  put='<!DOCTYPE html>'
  put='<html lang=\"en\">'
  put='  <head>'
  put='    <title>HTML test</title>'
  put='    <meta charset=\"utf-8\">'
  put='    <meta name=\"viewport\" content=\"width=device-width, user-scalable=no\">'
  put='    <link rel=\"shortcut icon\" href=\"data:image/x-icon;,\" type=\"image/x-icon\"> '
  put='    <style type=\"text/css\">'
  put='      html, body {'
  put='          margin: 0;'
  put='          height: 100%;'
  put='      }'
  put='    </style>'
  put='  </head>'
  put='  <body>'
  put='    <script type=\"module\" src=\"\"></script>'
  put='  </body>'
  put='</html>'
  " go to line 1
  :0
  :let linesToRemove=1
  :while linesToRemove > 0
  :  d
  :  let linesToRemove -=1
  :endwhile
:endfunction

"-------------------------------
" sections and headers
"-------------------------------
fun! SetSectionMacros()
  nnoremap <leader>- <Esc>i---------------------------------------------------------------------<CR><Esc>
  nnoremap <leader># <Esc>i#####################################################################<CR><Esc>
  nnoremap <leader>= <Esc>i=====================================================================<CR><Esc>

  " Don't allow full sections on these filetypes
  if (&ft=='js' || &ft=='html')
    return
  endif

  nnoremap <leader>s- <Esc>i---------------------------------------------------------------------<CR><CR>---------------------------------------------------------------------<Esc>ki
  nnoremap <leader>s# <Esc>i#####################################################################<CR><CR>#####################################################################<Esc>ki
  nnoremap <leader>s= <Esc>i=====================================================================<CR><CR>=====================================================================<Esc>ki
endfun

