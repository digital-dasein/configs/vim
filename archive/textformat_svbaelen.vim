" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0

"=====================================================================
" Custom functions
"=====================================================================
" Senne Van Baelen <senne@digitaldasein.org>
"---------------------------------------------------------------------
" Including:
"   - AlignText --> :AlignText <del> <tw>
"---------------------------------------------------------------------

function! ToggleAutowrap()
	if s:wrap  == 0
		let s:wrap = 1
		setlocal fo-=t
	else
		let s:wrap = 0
	endif
endfunction

function! IndentText(del, textwidth)
	let tw = a:textwidth
	let l:out = ""
	let l:idx = 1
	let [line_start, column_start] = getpos("'<")[1:2]
	let [line_end, column_end] = getpos("'>")[1:2]
	let lines = getline(line_start, line_end)
	if len(lines) == 0
		return ''
	endif

	let l:lnr = line_start

	let max_len_left = 0
	let list_nr_col_l = []

	" Calculate maximum length of left column
	for l:line in lines
		let this_line = "" . l:line . "\n"
		let split_string = split(this_line, a:del)
		let length = len(split_string)
		if length > 1
			let list_nr_col_l = list_nr_col_l + [l:lnr]
			if len(split_string[0]) > max_len_left
				let max_len_left = len(split_string[0])
			endif
		endif

		let l:lnr += 1
	endfor

	"echo list_nr_col_l
	"
	let max_len_left = max_len_left+1

	" Split text into columns

	let l:lnr = line_start
	let newline = ""

	" Loop again over all lines
	for l:line in lines
		let this_line = "" . l:line . "\n"
		let split_string = split(this_line, a:del)
		let length = len(split_string)
		if length > 2
			let split_string = [split_string[0], split_string[1:]]
		endif

		if len(this_line) == 1
			let l:out .= "\n"

			" with left column
		elseif length > 1
			let words_right = split(split_string[1], " ")
			"let l:out .= "" . split_string[0] . "\n"
			let extra_space_left = max_len_left - len(split_string[0])
			let s_left = split_string[0]

			let add_space = 0
			while add_space < extra_space_left
				let add_space += 1
				let s_left .= " "
			endwhile

			let space_right = tw - max_len_left
			let s_right = ""
			let space_available = space_right
			let idx = 0
			while len(s_right) < space_available && idx < len(words_right)
				let word = substitute(words_right[idx], '\n\+$', '', '')
				if idx == 0
					let s_right .= " " . word . ""
				else
					let s_right .= " " . word . ""
				endif
				let idx += 1
			endwhile

			let l:out .= "" . s_left . ":" . s_right . "\n"

			" overshoot in words
			let nr_extra_words = len(words_right)-idx
			let newline = ""
			let count_extra = 0
			if nr_extra_words > 0
				while count_extra < nr_extra_words
					if count_extra == 0
						let add_space = 0
						while add_space < max_len_left + 1
							let add_space += 1
							let newline .= " "
						endwhile
					endif

					let wordindex = nr_extra_words - count_extra

					let word = substitute(words_right[-wordindex], '\n\+$', '', '')
					if count_extra == 0
						let newline .= " " . word . ""
					else
						let newline .= " " . word . ""
					endif
					let count_extra += 1
				endwhile
			endif


			" Without left column
		else
			let words_right = split(split_string[0], " ")
			let words_added = []

			if len(newline) == 0
				let s_right = ""
				let add_space = 0
				while add_space < max_len_left + 2
					let add_space += 1
					let s_right .= " "
				endwhile
			else
				let s_right = substitute(newline, '\n\+$', '', '')
			endif

			let idx = 0

			while len(words_added) < len(words_right)
				while len(s_right) < tw && idx < len(words_right)
					let word = substitute(words_right[idx], '\n\+$', '', '')
					let words_added = words_added + [word]

					if len(s_right) == 0 && idx == 0
						let s_right .=  "" . word . ""
					else
						let s_right .=  " " . word . ""
					endif
					let idx += 1

				endwhile

				" check next line
				if l:lnr < line_end
					let next_line = "" . getline(l:lnr+1, l:lnr+1)[0] . "\n"
					let len_word_next = len(split(next_line, ' ')[0] + 1)
				else
					let len_word_next = 0
				endif

				" last part of line
				if idx == len(words_right) && len(next_line) == 1
					" next line is a empty one, so push
					let l:out .= "" . s_right . "\n"
				elseif idx == len(words_right) && len(newline) < tw-len_word_next
					" still room left, so don't push yet
					let newline = s_right
				else
					let l:out .= "" . s_right . "\n"
					let s_right = ""
					let add_space = 0
					while add_space < max_len_left + 1
						let add_space += 1
						let s_right .= " "
					endwhile
				endif
			endwhile
		endif
		let l:lnr += 1
	endfor

	" let [line_start, column_start] = getpos("'<")[1:2]
	"let [line_end, column_end] = getpos("'>")[1:2]

	exec 'call cursor(' line_start ',' column_start ')'
	exec 'normal v'
	exec 'call cursor(' line_end ',' column_end ')'
	exec 'normal delete'

	let split_out = split(l:out, '\n')
	for line in split_out
		exec 'call setline(".",' string(line) ')'
		exec 'normal 0o'
	endfor

	"echo l:out
	return

endfunction

"command! -range IndentText call IndentText()
command! -range -nargs=* AlignText call IndentText(<f-args>)
"call like this ->  :AlignText del tw
