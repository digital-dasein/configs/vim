" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0

" File-specific mappings (or their function wrappers) are often found in the
" macros directory; they can the be called in an autocmd, or set globally (if
" configured as such)

"---------------------------------------------------------------------
" Plugins
"---------------------------------------------------------------------

" see separate file: vimext_svbaelen_plugins.vim

"---------------------------------------------------------------------
" Fuzzy finder
"---------------------------------------------------------------------

"<https://github.com/junegunn/fzf.vim>
"Installed fuzzy finder through git
"https://vimawesome.com/plugin/fzf#installation
set rtp+=~/.fzf
nnoremap <leader><space> :FZF! ~/ <cr>
nnoremap <C-t> :FZF!~/ <cr>>
"nnoremap <leader>a :FZF /  <cr>
"search in all buffers
noremap <leader>S :Lines <cr>
"search in this buffer (search)
nnoremap <leader>s :BLines <cr>
nnoremap <leader>h :History  <cr>

" show all open buffers and their status (fzf plugin)
" (this overwrites the standard buffer show mapping)
nmap <leader>b :Buffer<CR>

" hide preview by default, toggle with CTRP-P
let g:fzf_preview_window = ['right:50%:hidden', 'ctrl-P']

let g:fzf_layout = {
      \ 'down': '80%',
      \}

let g:fzf_history_dir = '~/.local/share/fzf-history'

"---------------------------------------------------------------------
" Markdown plugin
"---------------------------------------------------------------------

" Markdown markdown
set conceallevel=2
" disable concealing
"let g:vim_markdown_conceal = 0
" no concealing in latex math model
let g:tex_conceal = ""
let g:vim_markdown_math = 1
" no concealing in code blocks
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_fenced_languages = ['csharp=cs']

" shortcut function to enable folding
fun! MdSetFold()
  "let g:vim_markdown_folding_disabled = 0
  execute 'edit'
  execute ':normal G'
  execute ':normal zO'
  execute ':normal gg'
endfun
command! MdFold call MdSetFold()
command! MDFold call MdSetFold()
command! MdSetFold call MdSetFold()
command! MDSetFold call MdSetFold()

" Enable/disable folding
" disable folding from plugin (often annoying behaviour)
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_no_extensions_in_markdown = 1
" enable built-in md folding, customised in autocmd
let g:markdown_folding = 1

"---------------------------------------------------------------------
" YouCompleteMe
"---------------------------------------------------------------------

"For 'YouCompleteMe'
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
let g:ycm_key_list_select_completion = ['<Down>']
let g:ycm_key_list_previous_completion = ['<Up>']

"Some general completion pop up changes (otherwise YCM interfered with
"SnipMate):

if has('gui_running') "because in terminal this complicates the arrow keys
  functions
  inoremap <expr> <Esc>      pumvisible() ? "\<C-e>" : "\<Esc>"
endif

inoremap <expr> <CR>       pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <expr> <Down>     pumvisible() ? "\<C-n>" : "\<Down>"
inoremap <expr> <Up>       pumvisible() ? "\<C-p>" : "\<Up>"
inoremap <expr> <PageDown> pumvisible() ? "\<PageDown>\<C-p>\<C-n>" :
"\<PageDown>"
inoremap <expr> <PageUp>   pumvisible() ? "\<PageUp>\<C-p>\<C-n>" :
"\<PageUp>"
"
" you still have to go to the end of the file
" otherwise <D-y> will have another (edit) function
nnoremap <D-y> :let g:ycm_auto_trigger=0<CR>                "turn off YCM
nnoremap <D-Y> :let g:ycm_auto_trigger=1<CR>                "turn on YCM

"---------------------------------------------------------------------
" Surround
"---------------------------------------------------------------------

" HTML surround mappings are defined in marcros/macros_svbaelen_html.vim

"---------------------------------------------------------------------
" Latex
"---------------------------------------------------------------------
set grepprg=grep\ -nH\ $*
let g:tex_comment_nospell=1

"FOR SAVING MACROS: shortcut
cmap w!! %!sudo tee > /dev/null

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults
" to 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':

let g:tex_flavor='latex'
let g:Tex_TreatMacViewerAsUNIX = 1
let g:Tex_ExecuteUNIXViewerInForeground = 1
let g:Tex_ViewRule_ps = 'xdg-open'
let g:Tex_ViewRule_pdf = 'xdg-open'
let g:Tex_ViewRule_dvi = 'xdg-open'
"let g:Tex_FormatDependency_dvi = 'dvi,ps,pdf'"
let g:Tex_DefaultTargetFormat='pdf'

"Don't open files if error automatically
let g:Tex_GotoError=0
let g:Tex_MultipleCompileFormats='pdf,bib,dvi,pdf'
"let g:Tex_MakeIndexFlavor = 'makeindex $*.idx, makeindex $*.nlo, makeindex
"$*.nls'

"nmap <Leader>f :call MakeTexFolds(1) <CR>

""TRY TO USE W/ BIBER (it works, but rather cumbersome)
""https://tex.stackexchange.com/questions/83715/biber-backend-and-vim-latex
"let g:Tex_BibtexFlavor = 'biber'

"map <Leader>lb :<C-U>exec '!biber '.Tex_GetMainFileName(':p:t:r')<CR>

"---------------------------------------------------------------------
" Ale
"---------------------------------------------------------------------

" check current enabled linters and configuration (variables):
"  :ALEInfo
" (!! different for every filetype, obviously)

" https://www.reddit.com/r/vim/comments/b9amj9/how_do_i_get_rid_of_ale_errors_on_the_line_i_am/
let g:ale_virtualtext_cursor=0
nnoremap <leader>t :ALEToggle <CR>

" Note: this requires the user to have the linters (e.g. flake8, pylint,
" ...) instlalled on the user's system.
"
" $ which flake8 -> /usr/bin/flake8
" When pylint is installed (pip package):
" -> $ which pylint -> $HOME/.local/bin/pylint
"
" PYLINT CONFIG:
" -> ~/.pylintrc"
"
" other (locally) availble latex options:
" alex.vim (unset here, because the check for "profanities" can be a bit too
" low-treshold in my opinion

"let g:ale_python_flake8_auto_pipenv = 0
"let g:ale_python_flake8_change_directory = 1
"let g:ale_python_flake8_executable = 'flake8'
"let g:ale_python_flake8_options = ''
"let g:ale_python_flake8_use_global = 0
"let g:ale_python_mypy_auto_pipenv = 0
"let g:ale_python_mypy_executable = 'mypy'
"let g:ale_python_mypy_ignore_invalid_syntax = 0
"let g:ale_python_mypy_options = ''
"let g:ale_python_mypy_use_global = 0
"let g:ale_python_pylint_auto_pipenv = 0
"let g:ale_python_pylint_change_directory = 1
"let g:ale_python_pylint_executable = 'pylint' let
"g:ale_python_pylint_options = ''
"let g:ale_python_pylint_use_global = 0
"
"let g:ale_python_pylint_executable = ''


"C/CPP
":help ale-cpp-options

let g:ale_c_parse_makefile=1
"let g:ale_c_gcc_options = '-std=gnu11 -Wall -I/usr/include -I/usr/local/include -I../include -I../../include'
let g:ale_c_gcc_options = '-std=gnu11 -Wall -I/usr/include -I/usr/local/include -I../include -I../../include -I/usr/lib/x86_64-linux-gnu'
let g:ale_c_cc_options = '-std=gnu11 -Wall -I/usr/include -I/usr/local/include -I../include -I../../include -I/usr/lib/x86_64-linux-gnu -Wno-comment'

" I currently use gcc as 'compiler option', so only the gcc linter is
" relevant
let g:ale_cpp_cpplint_options=1
"FOR MOOS (TODO: detect automatically)
"Make sure gcc can detect all the headers
let g:ale_cpp_cc_options = '-std=c++17 -Wall'

let g:ale_rust_analyzer_config = {
      \  'diagnostics': {
      \    'disabled': ['unused_braces', 'redundant_field_names'],
      \  }
      \}

"CPP options for compiler
"https://github.com/dense-analysis/ale#5xv-how-can-i-configure-my-c-or-c-project

" Current Filetype: cpp
" Available linters: ['ccls', 'clang', 'clangcheck', 'clangd', 'clangtidy',
"                     'clazy', 'cppcheck', 'cpplint', 'cquery',
"                     'flawfinder', 'gcc']
"
" HTML available: ['alex', 'fecs', 'htmlhint', 'proselint', 'stylelint',
"                  'tidy', 'writegood'],
" Disable alex (catches insensitive, inconsiderate writing)
"<github.com/get-alex/alex>


"Javscript
" ESLint configured by default
" Best to add rule to local .eslintrc.{yml,json,...} to make unused
" variables a warning instead of error:
" <https://stackoverflow.com/a/59225562>

" ALE styling
"let g:ale_sign_error = '✘'
"let g:ale_sign_warning = '⚠'
"highlight ALEErrorSign ctermbg=NONE ctermfg=red
"highlight ALEWarningSign ctermbg=NONE ctermfg=yellow
"
"help ale-java-options
"let g:ale_java_javac_classpath = ''

"\   'c':['cc', 'ccls', 'clangcheck', 'clangd', 'clangtidy', 'cppcheck', 'cpplint'],
"\   'python': ['flake8', 'mypy', 'pylint', 'pyright'],
"
" SET ALE LINTERS:
let g:ale_linters = {
\   'rust': ['analyzer', 'cargo', 'rls'],
\   'cpp':['gcc','clangtidy','clang','clangcheck','cpplint','cppcheck',
\          'ccls','clangd','clangcheck','claze'],
\   'html':['fecs', 'htmlhint', 'proselint', 'stylelint', 'tidy',
\           'writegood'],
\   'tex':['chktex'],
\   'python': ['flake8', 'mypy', 'pylint', 'pyright'],
\   'typescript': ['cspell', 'eslint', 'fecs', 'flow', 'flow-language-server', 'jscs', 'jshint', 'standard', 'tsserver', 'xo'],
\}

" https://github.com/dense-analysis/ale/issues/208#issuecomment-274142439
let g:ale_python_pylint_options = "--init-hook='import sys; sys.path.append(\".\")'"

" :help ale-rust-cargo and/or :help ale-rust-analyzer
let g:ale_rust_cargo_default_feature_behavior = "all"

" Typescript linters 2023-02-20
" Availabel: 'cspell', 'deno', 'eslint', 'fecs', 'flow',
"            'flow-language-server', 'jscs', 'jshint', 'standard', 'tsserver',
"            'xo'
" by default do not use the DENO linter.
" TODO: find a way to invoke DENO linter for DENO scripts

"Currently ignore typescript standard linter (eslint does the same anyway) as
"ALE seesm to struggle with pnpm
"<https://github.com/dense-analysis/ale/issues/4234>
let g:ale_linters_ignore = {
\   'typescript': ['standard'],
\}


let g:ale_fixers = {}
let g:ale_fixers.python = ['black']
let g:ale_python_auto_pipenv = 0


" Disable auto-detection of virtualenvironments
let g:ale_virtualenv_dir_names = []
" Environment variable ${VIRTUAL_ENV} is always used

"---------------------------------------------------------------------
" SnipMate
"---------------------------------------------------------------------

let g:snipMate = {
            \   'snippet_version' : 1,
            \   'override' : 1,
            \   'always_choose_first' : 1,
            \   'no_match_completion_feedkeys_chars' : '',
            \ }
