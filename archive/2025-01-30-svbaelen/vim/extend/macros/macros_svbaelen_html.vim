" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0
"
" default HTML macros svbaelen
" additional html macros in separate files:
"   - ./macros_svbaelen_htmlbook.vim
"
" by default, do not set anything here (rather in autocmd)
let s:set_mappings = 0      " define mappings in here (1, or elsewhere: 0)

" overwrite by global
if g:set_g_html
  let s:set_mappings = 1
endif

"---------------------------------------------------------------------
" Function wrappers
"---------------------------------------------------------------------
"
function! HtmlNewFileMinimal()
  put='<!DOCTYPE html>'
  put='<html lang=\"en\">'
  put='  <head>'
  put='    <title>HTML test</title>'
  put='    <meta charset=\"utf-8\">'
  put='    <meta name=\"viewport\" content=\"width=device-width, user-scalable=no\">'
  put='    <link rel=\"shortcut icon\" href=\"data:image/x-icon;,\" type=\"image/x-icon\"> '
  put='    <style type=\"text/css\">'
  put='      html, body {'
  put='          margin: 0;'
  put='          height: 100%;'
  put='      }'
  put='    </style>'
  put='  </head>'
  put='  <body>'
  put='    <script type=\"module\" src=\"\"></script>'
  put='  </body>'
  put='</html>'
  " go to line 1
  :0
  :let linesToRemove=1
  :while linesToRemove > 0
  :  d
  :  let linesToRemove -=1
  :endwhile
:endfunction

function SetHtmlMappings()
  nnoremap <leader>nh :call HtmlNewFileMinimal()<CR>

  " Surround (plugin needs to be loaded!)
  vmap Sc S<code>
  vmap Sp S<p>
  vmap Sa S<a href="">v$:s/href=""/href="<++>"/ <CR><C-j>
  vmap Sb S<b>
  vmap Si S<i>
  vmap Ss S<samp>
  vmap Sl S<li>
  " wrap every line of selection with <samp></samp>
  vmap SS :normal yss<samp><CR>
  " wrap every line of selection with <li></li>
  vmap SL :normal yss<li><CR>
:endfunction

"---------------------------------------------------------------------
"Mappings
"---------------------------------------------------------------------

if s:set_mappings
  call SetHtmlMappings()
endif
