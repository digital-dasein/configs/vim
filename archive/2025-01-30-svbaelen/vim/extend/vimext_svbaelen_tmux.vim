" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0

" check that we are _inside_ a tmux session
let tmux = $TMUX

if strchars(tmux) > 0
  " use list to avoid enter input
  if $TMUX_COLORSCHEME == ""
    set background=dark
    colorscheme ron
  else
    let current_scheme = systemlist("tmux show-environment -g TMUX_COLORSCHEME | sed 's:^.*=::'")[0]
    if current_scheme == "black"
      set background=dark
      colorscheme ron
    elseif current_scheme == "white"
      set background=light
      colorscheme PaperColor
    endif
  endif
else
  set background=dark
  colorscheme ron
endif
