" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0
"
" Filetype autocmds

" default (background = same as terminal)
au BufEnter,BufNewFile,BufRead *
      \ colorscheme $DEFAULT_VIM_COLORSCHEME |
      \ highlight LineNr ctermfg=244 |
      \ highlight Comment ctermfg=244 |
      \ highlight Normal ctermfg=231 ctermbg=none |
      \ highlight EndOfBuffer ctermbg=none |
      \ highlight Type ctermfg=121 |
      \ highlight Identifier cterm=bold ctermfg=87 |
      \ highlight Special ctermfg=191 |
      \ highlight Statement ctermfg=191 |
      \ highlight PreProc ctermfg=81 |
      \ highlight Constant ctermfg=87 |
      \ highlight SpecialComment ctermfg=194 |
      \ highlight MatchParen ctermbg=245 |
      \ highlight String ctermfg=144 |
      \ highlight Cursor ctermbg=255 |
      \ call UpdateLightlineTheme("nord") |
      \ highlight ALEWarningSign ctermbg=yellow ctermfg=black |
      \ highlight ALEErrorSign ctermbg=red ctermfg=black |

au BufEnter,BufNewFile,BufRead ~/.vim/**/vim_*,*.vim,*vimrc
      \ setlocal nospell |
      \ set syntax=vim |
      \ set filetype=vim |
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2

" Hidden filen
autocmd BufEnter,BufNewFile,BufRead .*
      \ setlocal nospell |
      \ :syn match UrlNoSpell '\w\+:\/\/[^[:space:]]\+' contains=@NoSpell

" avoid scanning include files for completion in C++
" <https://superuser.com/a/77917>
au BufEnter,BufNewFile,BufRead *.c,*.cpp,*.h
      \ set complete-=i |
      \ setlocal nospell

au BufEnter,BufNewFile,BufRead *.c,*.cpp,*.h
      \ setlocal nospell |
      \ set shiftwidth=2 |
      \ set textwidth=79 |
      \ set autoindent |

au BufEnter,BufNewFile,BufRead *.rs,*.sql
      \ highlight Identifier ctermfg=255 |
      \ setlocal nospell |
      \ set shiftwidth=4 |
      \ set textwidth=79 |
      \ set cino+=(0 |
      \ set autoindent |

au BufEnter,BufNewFile,BufRead *.toml
      \ setlocal nospell |


au BufEnter,BufNewFile,BufRead *.sqle
      \ set filetype=sql |
      \ setlocal nospell |
      \ set shiftwidth=4 |
      \ set textwidth=79 |
      \ set autoindent


au BufEnter,BufNewFile,BufRead *.py
      \ set tabstop=4 |
      \ set softtabstop=4 |
      \ set shiftwidth=4 |
      \ set textwidth=79 |
      \ set expandtab |
      \ set autoindent |
      \ set fileformat=unix |
      \ set cinoptions=:0,g0,(0,W4,m1 |
      \ match Type /self/

au BufEnter,BufRead,BufRead *.js,*.py,*.ts,*json
      \ let background=$DEFAULT_VIM_BACKGROUND |
      \ call UpdateLightlineTheme("nord") |
      \ setlocal nospell |
      \ set tw=79 |
      \ highlight ALEWarningSign ctermbg=yellow ctermfg=black |
      \ highlight ALEErrorSign ctermbg=red ctermfg=black |
      "\ let g:ale_linters = { 'typescript': ['eslint', 'tsserver', 'typecheck'] }

" fix bug
au BufEnter,BufRead,BufRead *.tsx
      \ highlight link typescriptReserved     keyword



" fix bug in "/usr/share/vim/vim82/syntax/typescriptcommon.vim", i.e.,
" typescriptReserved syntax resolves to Error hl, while one could argue for
" Keyword in that case. Or, just use JS syntax for convenience (pick one)
au BufEnter,BufRead,BufRead *.ts
      "\ set syntax=javascript
      \ set sw=2 |
      \ hi def link typescriptReserved  Keyword |
      \ set syntax=typescript

au BufEnter,BufNewFile,BufRead *.c*
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2 |
      \ set textwidth=79 |
      \ set expandtab |
      \ set autoindent

au FileType markdown set foldlevel=1
au BufEnter,BufRead,BufNewFile *.md
      \ colorscheme slate_svb_md |
      \ highlight Normal ctermfg=231 ctermbg=none |
      \ highlight EndOfBuffer ctermbg=none |
      \ call UpdateLightlineTheme("nord") |
      \ set textwidth=150 |
      \ set spell spelllang=en_gb |
      "\ setlocal formatoptions-=l formatoptions+=wa |
      "\ nnoremap <silent> zC zC :set foldlevel=2<CR> |
      "\ vnoremap <silent> zC zC :set foldlevel=2<CR> |
      \ call SetMarkdownSurroundMappings() |
      "\ nnoremap <silent> zc zc :set foldlevel=2<CR> |
      "\ vnoremap <silent> zc zc :set foldlevel=2<CR>
      " for some reason, these format options have no effect after a comment
      "\ execute ':AirlineToggleWhitespace' |
      "\ let b:airline#extensions#whitespace#enabled=0 |
      " default qlnrt |
      "\ qnrtw
      "\ set noautoindent |
      "\ set formatoptions+=w |
      "\ execute ':normal G' |
      "\ execute ':normal zO'|
      "\ execute ':normal gg' |

"" airline trailing whitespace error (toggle)
"autocmd FileType * unlet! g:airline#extensions#whitespace#checks
"autocmd FileType markdown let g:airline#extensions#whitespace#checks = [ 'indent' ]
"autocmd BufNewFile,BufRead /tmp/mutt* let g:airline#extensions#whitespace#checks = [ 'indent' ]

"\ syntax region htmlFold start="<\z(\<\(area\|base\|br\|col\|command\|embed\|hr\|img\|input\|keygen\|link\|meta\|para\|source\|track\|wbr\>\)\@![a-z-]\+\>\)\%(\_s*\_[^/]\?>\|\_s\_[^>]*\_[^>/]>\)" end="</\z1\_s*>" fold transparent keepend extend containedin=htmlHead,htmlH\d |

" optional (not used anymore) in HTML
"\ let NERDCustomDelimiters = {
  "\   'javascript': { 'left': '<!--','right': '-->' }
  "\ } |

au BufEnter,BufNewFile,BufEnter *.html,
      \ set spell spelllang=en_gb |
      \ SnipMateLoadScope html |
      \ nnoremap <leader>ch :call RunConcatHtml()<CR>|
      \ nnoremap <leader>R :call RunConcatHtml()<CR>|
      \ nnoremap <leader>r :call RunConcatHtmlSwitch()<CR><CR> |
      \ nnoremap <S-r> :call RunConcatHtmlSwitch()<CR><CR> |
      \ set textwidth=150 |
      \ colorscheme slate_svb |
      \ call UpdateLightlineTheme("nord") |
      \ call SetHtmlMappings() |
      \ call SetHtmlBookMappings() |
      \ set foldmethod=marker |
      \ set foldmarker=<h2>,<dd-slide> |
      \ let g:loaded_matchparen = 1

" consider as HTML
au BufEnter,BufNewFile,BufEnter index.rocket.js,*.astro
      \ SnipMateLoadScope html |
      \ set sw=2 |
      \ nnoremap <leader>ch :call RunConcatHtml()<CR>|
      \ nnoremap <leader>R :call RunConcatHtml()<CR>|
      \ nnoremap <leader>r :call RunConcatHtmlSwitch()<CR><CR> |
      \ nnoremap <S-r> :call RunConcatHtmlSwitch()<CR><CR> |
      \ set textwidth=150 |
      \ set sw=2 |
      \ call UpdateLightlineTheme("nord") |
      \ call SetHtmlMappings() |
      \ call SetHtmlBookMappings()


au BufEnter,BufNewFile,BufEnter index.rocket.md,
      \ SnipMateLoadScope html |
      \ call SetHtmlMappings()|
      \ set sw=2 |
      \ nnoremap <S-r> :call RunConcatHtmlSwitch()<CR><CR> |
      \ set textwidth=150 |

"au BufEnter,BufNewFile,BufRead *.html,*.js,*.css
      "\ set textwidth=150 |
      "\ set tabstop=2 |
      "\ set softtabstop=2 |
      "\ set shiftwidth=2 |
      "\ set foldmethod=marker |
      "\ set foldmarker=[[[,]]] |
      "\ normal zM |

au BufEnter,BufNewFile,BufRead *.txt
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2

au BufEnter,BufNewFile,BufRead CMakeLists.txt
      \ setlocal nospell

au BufEnter,BufNewFile,BufRead *.css
      \ set tabstop=4 |
      \ set softtabstop=4|
      \ set shiftwidth=4 |
      \ set foldmethod=marker |
      \ set foldmarker=[[[,]]]

au BufNewFile,BufRead,BufEnter *.sh
      \ set tabstop=4 |
      \ set softtabstop=4 |
      \ set shiftwidth=4 |
      \ call UpdateLightlineTheme("nord") |
      \ setlocal nospell

" Note: for .tex files "\ref" is included in re RefGroup, however, href is
" not (/usr/share/vim/vim80/syntax/tex.vim) --> copy command for \ref to
" \href in autocmd (.tex)

au BufEnter,BufNewFile,BufRead *.tex
      \ set spell spelllang=en_gb |
      \ set tabstop=2 |
      \ syn region texRefZone		matchgroup=texStatement start="\\v\=href{"
      \	end="}\|%stopzone\>"	contains=@texRefGroup |
      \ colorscheme slate_svb |
      \ call UpdateLightlineTheme("nord") |
      \ set softtabstop=2|
      \ set shiftwidth=2 |
"Replace spaces with hyphens (e.g. best practice in LaTeX) in current line |
      \ nnoremap <leader>s 0 v $ :s/\%V /-/g <CR>


au BufNewFile,BufRead *.bib
      \ setlocal nospell

      "\ let g:syntastic_mode_map = { 'mode': 'passive','active_filetypes': [],'passive_filetypes': [] }

      "\ set nojs |
      "\ set fo=watqc |
" normally, comments for "mail" is set like this:
" set comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-

autocmd BufEnter,BufNewFile,BufRead /tmp/mutt*,*.eml
      \ set spell spelllang=nl,en_gb |
      \ set tw=79 |
      \ set nojs |
      \ set fo=watqc |
      \ hi clear SpellBad |
      \ set noautoindent |
      \ set comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:> |
      \ hi SpellBad cterm=underline ctermfg=red  |
      \ match ErrorMsg '\s\+$' |
      \ hi ErrorMsg ctermbg=108 |
      \ :ALEToggle |
      \ map q <Nop> |
      "\ Email signatures |
      \ nnoremap <leader>ss <Esc>iSenne Van Baelen<CR>--<CR>Department of Mechanical Engineering, KU Leuven<CR>Andreas Vesaliusstraat 13 - box 2600<CR>3000 Leuven, Belgium<CR>Intelligent Mobile Platforms (IMP) research group<CR>https://www.mech.kuleuven.be/imp<Esc>i |
      \ nnoremap <leader>S <Esc>iSenne Van Baelen<CR>--<CR>Department of Mechanical Engineering, KU Leuven<CR>Andreas Vesaliusstraat 13 - box 2600<CR>3000 Leuven, Belgium<CR>Intelligent Mobile Platforms (IMP) research group<CR>https://www.mech.kuleuven.be/imp<Esc>i |

autocmd BufEnter,BufNewFile,BufRead *.eml
      \ silent! call TrimWhitespace() |
      \ silent! call RemoveTrimWhitespace() |
      \ norm 8G

autocmd BufEnter,BufNewFile,BufRead *.mjs,*js,*cjs
      \ let g:ale_linters_ignore={ 'javascript': ['deno'] }

au BufNewFile,BufRead *.map
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2 |
      \ set textwidth=79 |
      \ set expandtab |
      \ set autoindent |
      \ set fileformat=unix |
      \ set syntax=python

au BufEnter,BufRead,BufNewFile *.geojson
      \ set syntax=json

au BufEnter,BufNewFile,BufRead *.jsonld
      \ set syntax=json

au BufEnter,BufNewFile,BufRead *.conf,*.yaml,*.yml,*.css
      \ call UpdateLightlineTheme("nord") |
      \ setlocal nospell |

" command on all mutt files to remove
au VimLeave /tmp/mutt/neomutt-* !/home/svbaelen/Documents/personal/scripts/mail-related/join.py %
au VimLeave *.eml !/home/svbaelen/Documents/personal/scripts/mail-related/join.py %
