" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0
"
" in case I want to toggle ALE for a certain filetype:
"	Filenames without extensions: checks if the filename contains a ., and
"	if not, switches to a given syntax:

au BufEnter,BufNewFile,BufRead ~/.vim/**/vim_*
      \ set syntax=vim |
      \ set filetype=vim

autocmd BufNewFile,BufRead *
      \	if expand('%:t') !~ '\.'  |
      \ setlocal nospell |
      \ endif

" Hidden filen
autocmd BufNewFile,BufRead .*
      \ setlocal nospell |
      \ :syn match UrlNoSpell '\w\+:\/\/[^[:space:]]\+' contains=@NoSpell

au BufNewFile,BufRead *.py
      \ set tabstop=4 |
      \ set softtabstop=4 |
      \ set shiftwidth=4 |
      \ set textwidth=79 |
      \ set expandtab |
      \ set autoindent |
      \ set fileformat=unix |
      \ match Type /self/
      "\ colorscheme gruvbox |
      "\ set background=dark

au BufEnter,BufRead *.js,*.py,*.ts,*json
      \ let &background=$DEFAULT_VIM_BACKGROUND |
      \ colorscheme $DEFAULT_VIM_COLORSCHEME |
      \ AirlineTheme luna |
      \ setlocal nospell |
      \ set tw=76 |
      \ highlight ALEWarningSign ctermbg=yellow ctermfg=black |
      \ highlight ALEErrorSign ctermbg=red ctermfg=black


au BufNewFile,BufRead *.c*
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2 |
      \ set textwidth=77 |
      \ set expandtab |
      \ set autoindent

au BufEnter,BufRead,BufNewFile *.md
      \ setlocal formatoptions-=l formatoptions+=wa |
      \ colorscheme slate |
      \ AirlineTheme luna |
      \ call GetModSlate() |
      \ set textwidth=79 |
      \ call UpdateMdColors() |
      \ set spell spelllang=en_gb |
      \ call RemoveTrimWhitespace() |
      " for some reason, these format options have no effect after a comment
      "\ execute ':AirlineToggleWhitespace' |
      "\ let b:airline#extensions#whitespace#enabled=0 |
      " default qlnrt |
      "\ qnrtw
      "\ set noautoindent |
      "\ set formatoptions+=w |
      "\ execute ':normal G' |
      "\ execute ':normal zO'|
      "\ execute ':normal gg' |

" airline trailing whitespace error (toggle)
autocmd FileType * unlet! g:airline#extensions#whitespace#checks
autocmd FileType markdown let g:airline#extensions#whitespace#checks = [ 'indent' ]
autocmd BufNewFile,BufRead /tmp/mutt* let g:airline#extensions#whitespace#checks = [ 'indent' ]

" VIM GRID 1 (best also do a select + s/<CR> /<CR>/g)
"------------------------------------------------------
"<Esc>i
"<div class="gr-1" style="grid-template-columns: %"><CR>
"   <div style="padding:0 0 0 0;"><CR>
"     <CR>
"   </div><CR>
"</div><!-- end grid --><Esc>

" VIM GRID 2 (best also do a select + s/<CR> /<CR>/g)
"------------
"<Esc>i
"<div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR>
"   <div style="padding:0 0 0 0;"><CR>
"     <++><CR>
"   </div><CR>
"   <div style="padding:0 0 0 0;"><CR>
"   </div><CR>
"</div><!-- end grid --><Esc>

" VIM GRID 3 (best also do a select + s/<CR> /<CR>/g)
"------------
"<Esc>i
"<div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR>
"   <div style="padding:0 0 0 0;"><CR>
"     <++><CR>
"   </div><CR>
"   <div style="padding:0 0 0 0;"><CR>
"     <++><CR>
"   </div><CR>
"   <div style="padding:0 0 0 0;"><CR>
"     <++><CR>
"   </div><CR>
"</div><!-- end grid --><Esc>

" VIM GRID IMAGE + CAPTION
"------------
"<Esc>i
"<div style="padding:0 0 0 0;"><CR>
"  <img width="100%" src="figures/<++>"><CR>
"  <div class="img-caption"><CR>
"    <span><b>Fig. </b>: <++></span><CR>
"  </div><CR>
"</div>

au BufNewFile,BufEnter *.html
      \ set spell spelllang=en_gb |
      \ nnoremap <leader>ch :call RunConcatHtml()<CR>|
      \ nnoremap <leader>R :call RunConcatHtml()<CR>|
      \ nnoremap <leader>r :call RunConcatHtmlSwitch()<CR><CR> |
      \ nnoremap <S-r> :call RunConcatHtmlSwitch()<CR><CR> |
      \ set textwidth=150 |
      \  colorscheme slate |
      \ AirlineTheme luna |
      \ call GetModSlate() |
      \ call UpdateHtmlColors()

au BufNewFile,BufRead *.html,*.js,*.css
      \ set textwidth=150 |
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2 |
      \ set foldmethod=marker |
      \ set foldmarker=[[[,]]] |
      "\ nnoremap <leader>n <Esc>i<!--================ SLIDE [[[ ================--><CR><section><CR><++><CR></section><CR><!--]]]--> <Esc>kkkkwwhi
      \ nnoremap <leader>n <Esc>i <!-- SLIDE  [[[ --><CR><section class="slide"><CR><++><CR><my-footer></my-footer><CR></section><CR><!--]]]--> <Esc>kkkkkv0ojjjjj$=<Esc>kwwwhi|
      \ nnoremap <leader>g1 <Esc>i <div class="gr-1" style="grid-template-columns: <++>%"><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR></div><!-- end grid --><Esc>kkkkv0ojjjj$=|
      \ nnoremap <leader>g2  <Esc>i <div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR></div><!-- end grid --><Esc>kkkkkkkkv0ojjjjjjjj$=|
      \ nnoremap <leader>g3 <Esc>i <div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR></div><!-- end grid --><Esc>kkkkkkkkkkkkv0ojjjjjjjjjjjj$=|
      \ nnoremap <leader>ic  <Esc>i <code class="ic"><++></code><Esc>v0o$=|
      \ nnoremap <leader>ib  <Esc>i <code class="ic"><++></code><Esc>v0o$=|
      \ nnoremap <leader>ct  <Esc>i <pre><code class="text"><++></code></pre><Esc>v0o$=|
      \ nnoremap <leader>c1  <Esc>i <figure id="<++>" class="cb-singleline ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR></figure><!--end listing--><Esc>kkv0ojjj$=|
      \ nnoremap <leader>cl  <Esc>i <figure id="<++>" class="cb-singleline ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR></figure><!--end listing--><Esc>kkv0ojjj$=|
      \ nnoremap <leader>cb  <Esc>i <figure id="<++>" class="listing ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR><div class="lcap"><++></div><CR></figure><!--end listing--><Esc>kkkv0ojjj$=|
      \ nnoremap <leader>lb  <Esc>i <figure id="<++>" class="listing ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR><div class="lcap"><++></div><CR></figure><!--end listing--><Esc>kkkv0ojjj$=|
      \ nnoremap <leader>l  <Esc>i <figure id="<++>" class="listing ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR><div class="lcap"><++></div><CR></figure><!--end listing--><Esc>kkkv0ojjj$=|
      \ nnoremap <leader>f <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=|
      \ nnoremap <leader>ff <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=|
      \ nnoremap <leader>i <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=|
      \ nnoremap <leader>ii <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=|
      \ nnoremap <leader>fi <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=|
      \ nnoremap <leader>im <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=|
      \ nnoremap <leader>f2 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkv0ojjjjjjjjjjjj$=|
      \ nnoremap <leader>i2 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkv0ojjjjjjjjjjjj$=|
      \ nnoremap <leader>f3 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkkkkkv0ojjjjjjjjjjjjjjjj$=|
      \ nnoremap <leader>i3 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkkkkkv0ojjjjjjjjjjjjjjjj$=|
      \ nnoremap <S-s> :call CreateSection()<CR>|
      \ nnoremap <leader>s :call CreateSection()<CR>|
      \ nnoremap <leader>e <Esc>i <figure id="<++>" class="equation" typeof="doco:FormulaBox"><CR>$$<CR><++><CR>$$<CR></figure><Esc>kkkkv0ojjjj$=|
      "\ nnoremap <leader>s1 :call NewHtmlSection()<CR>|
      "\ nnoremap <leader>s2 :call NewHtmlSection()<CR>|
      "\ nnoremap <leader>s3 :call NewHtmlSubSection()<CR>|
      "\ nnoremap <leader>s4 :call NewHtmlSubSubSection()<CR>|
      "\ nnoremap <leader>s5 :call NewHtmlSubSubSubSection()<CR>|
      \ nnoremap <leader>qb <Esc>i <blockquote class="quoteblock"><CR><++><CR></blockquote><Esc>kkv0ojj$=|
      \ nnoremap <leader>bq <Esc>i <blockquote class="quoteblock"><CR><++><CR></blockquote><Esc>kkv0ojj$=|
      \ normal zM |


" sections and headers
"-------------------------------
fun! SetSectionMacros()
  nnoremap <leader>- <Esc>i---------------------------------------------------------------------<CR><Esc>
  nnoremap <leader># <Esc>i#####################################################################<CR><Esc>
  nnoremap <leader>= <Esc>i=====================================================================<CR><Esc>

  " Don't allow full sections on these filetypes
  if (&ft=='js' || &ft=='html')
    return
  endif

  nnoremap <leader>s- <Esc>i---------------------------------------------------------------------<CR><CR>---------------------------------------------------------------------<Esc>ki
  nnoremap <leader>s# <Esc>i#####################################################################<CR><CR>#####################################################################<Esc>ki
  nnoremap <leader>s= <Esc>i=====================================================================<CR><CR>=====================================================================<Esc>ki
endfun

au BufNewFile,BufRead *
      \ call SetSectionMacros()


au BufNewFile,BufRead *.vim
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2

au BufNewFile,BufRead *.vimrc
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2

au BufNewFile,BufRead *.txt
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2

au BufNewFile,BufRead *.css
      \ set tabstop=4 |
      \ set softtabstop=4|
      \ set shiftwidth=4 |
      \ set foldmethod=marker |
      \ set foldmarker=[[[,]]]

au BufNewFile,BufRead,BufEnter *.sh
      \ set tabstop=4 |
      \ set softtabstop=4 |
      \ set shiftwidth=4 |
      \ colorscheme ron |
      \ AirlineTheme luna



" Note: for .tex files "\ref" is included in re RefGroup, however, href is
" not (/usr/share/vim/vim80/syntax/tex.vim) --> copy command for \ref to
" \href in autocmd (.tex)

au BufNewFile,BufRead *.tex
      \ set spell spelllang=en_gb |
      \ set tabstop=2 |
      \ syn region texRefZone		matchgroup=texStatement start="\\v\=href{"
      \	end="}\|%stopzone\>"	contains=@texRefGroup |
      \ colorscheme slate |
      \ AirlineTheme luna |
      \ call GetModSlate() |
      \ set softtabstop=2|
      \ set shiftwidth=2 |
"Replace spaces with hyphens (e.g. best practice in LaTeX) in current line |
      \ nnoremap <leader>s 0 v $ :s/\%V /-/g <CR>


au BufNewFile,BufRead *.bib
      \ setlocal nospell

      "\ let g:syntastic_mode_map = { 'mode': 'passive','active_filetypes': [],'passive_filetypes': [] }

      "\ set nojs |
      "\ set fo=watqc |
" normally, comments for "mail" is set like this:
" set comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-

autocmd BufNewFile,BufRead /tmp/mutt*
      \ call RemoveTrimWhitespace() |
      \ set spell spelllang=nl,en_gb |
      \ set tw=72 |
      \ set nojs |
      \ set fo=watqc |
      \ hi clear SpellBad |
      \ set noautoindent |
      \ set comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:> |
      \ hi SpellBad cterm=underline ctermfg=red  |
      \ match ErrorMsg '\s\+$' |
      \ hi ErrorMsg ctermbg=245 |
      \ :ALEToggle |
      \ map q <Nop> |
      "\ Email signatures |
      \ nnoremap <leader>ss <Esc>iSenne Van Baelen<CR>--<CR>Department of Mechanical Engineering, KU Leuven<CR>Andreas Vesaliusstraat 13 - box 2600<CR>3000 Leuven, Belgium<CR>Intelligent Mobile Platforms (IMP) research group<CR>https://www.mech.kuleuven.be/imp<Esc>i |
      \ nnoremap <leader>S <Esc>iSenne Van Baelen<CR>--<CR>Department of Mechanical Engineering, KU Leuven<CR>Andreas Vesaliusstraat 13 - box 2600<CR>3000 Leuven, Belgium<CR>Intelligent Mobile Platforms (IMP) research group<CR>https://www.mech.kuleuven.be/imp<Esc>i |

au BufNewFile,BufRead *.gd
      \ set tabstop=4 |
      \ set softtabstop=4 |
      \ set shiftwidth=4 |
      \ set textwidth=77 |
      \ set expandtab |
      \ set autoindent |
      \ set fileformat=unix

au BufNewFile,BufRead *.map
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2 |
      \ set textwidth=78 |
      \ set expandtab |
      \ set autoindent |
      \ set fileformat=unix |
      \ set syntax=python

au BufEnter,BufRead,BufNewFile *.geojson
      \ set syntax=json

au BufNewFile,BufRead *.jsonld
      \ set syntax=json

" not used for mutt (amymore)
"set noautoindent |
"set nosmartindent |

autocmd BufNewFile,BufRead *.pmwiki setf pmwiki

au BufEnter,BufNewFile,BufRead *.conf,*.yaml,*.yml,*.css
      \ colorscheme ron |
      \ AirlineTheme luna |
      \ setlocal nospell |


au BufNewFile,BufRead todo.md,tmp.md
      \ let g:ale_enabled = 0

function! HtmlSyntax()
  set syntax=html
  set textwidth=150
  set tabstop=2
  set softtabstop=2
  set shiftwidth=2
  set foldmethod=marker
  set foldmarker=[[[,]]]
  "nnoremap <leader>n <Esc>i<!--================ SLIDE [[[ ================--><CR><section><CR><++><CR></section><CR><!--]]]--> <Esc>kkkkwwhi
  nnoremap <leader>n <Esc>i <!-- SLIDE  [[[ --><CR><section class="slide"><CR><++><CR><my-footer></my-footer><CR></section><CR><!--]]]--> <Esc>kkkkkv0ojjjjj$=<Esc>kwwwhi
  nnoremap <leader>g1 <Esc>i <div class="gr-1" style="grid-template-columns: <++>%"><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR></div><!-- end grid --><Esc>kkkkv0ojjjj$=
  nnoremap <leader>g2  <Esc>i <div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR></div><!-- end grid --><Esc>kkkkkkkkv0ojjjjjjjj$=
  nnoremap <leader>g3 <Esc>i <div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR><div style="padding:0 0 0 0;"><CR><++><CR></div><CR></div><!-- end grid --><Esc>kkkkkkkkkkkkv0ojjjjjjjjjjjj$=
  nnoremap <leader>ic  <Esc>i <code class="ic"><++></code><Esc>v0o$=
  nnoremap <leader>ib  <Esc>i <code class="ic"><++></code><Esc>v0o$=
  nnoremap <leader>ct  <Esc>i <pre><code class="text"><++></code></pre><Esc>v0o$=
  nnoremap <leader>c1  <Esc>i <figure id="<++>" class="cb-singleline ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR></figure><!--end listing--><Esc>kkv0ojjj$=
  nnoremap <leader>cl  <Esc>i <figure id="<++>" class="cb-singleline ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR></figure><!--end listing--><Esc>kkv0ojjj$=
  nnoremap <leader>cb  <Esc>i <figure id="<++>" class="listing ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR><div class="lcap"><++></div><CR></figure><!--end listing--><Esc>kkkv0ojjj$=
  nnoremap <leader>lb  <Esc>i <figure id="<++>" class="listing ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR><div class="lcap"><++></div><CR></figure><!--end listing--><Esc>kkkv0ojjj$=
  nnoremap <leader>l  <Esc>i <figure id="<++>" class="listing ignore-ref<++>"><CR><pre><code class="<++>" data-lang="<++>"><++></code></pre><CR><div class="lcap"><++></div><CR></figure><!--end listing--><Esc>kkkv0ojjj$=
  nnoremap <leader>f <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>ff <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>i <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>ii <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>fi <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=
  nnoremap <leader>im <Esc>i <figure id="<++>" class="imageblock"><CR><img src="static/images/<++>" alt="<++>"><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkv0ojjj$=|
  nnoremap <leader>f2 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkv0ojjjjjjjjjjjj$=
  nnoremap <leader>i2 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-2" style="grid-template-columns: <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkv0ojjjjjjjjjjjj$=
  nnoremap <leader>f3 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkkkkkv0ojjjjjjjjjjjjjjjj$=
  nnoremap <leader>i3 <Esc>i <figure id="<++>" class="imageblock"><CR><div class="gr-3" style="grid-template-columns: <++>% <++>% <++>%"><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR><div style="padding:0 0 0 0;"><CR><img src="static/images/<++>" alt="<++>"><CR><div class="subcap"><++></div><CR></div><CR></div><CR><figcaption class="icap"><++></figcaption><CR></figure><!-- end imageblock --><Esc>kkkkkkkkkkkkkkkkv0ojjjjjjjjjjjjjjjj$=
  nnoremap <S-s> :call CreateSection()<CR>
  nnoremap <leader>s :call CreateSection()<CR>
  nnoremap <leader>e <Esc>i <figure id="<++>" class="equation" typeof="doco:FormulaBox"><CR>$$<CR><++><CR>$$<CR></figure><Esc>kkkkv0ojjjj$=
  " nnoremap <leader>s1 :call NewHtmlSection()<CR>
  " nnoremap <leader>s2 :call NewHtmlSection()<CR>
  " nnoremap <leader>s3 :call NewHtmlSubSection()<CR>
  " nnoremap <leader>s4 :call NewHtmlSubSubSection()<CR>
  " nnoremap <leader>s5 :call NewHtmlSubSubSubSection()<CR>
  nnoremap <leader>qb <Esc>i <blockquote class="quoteblock"><CR><++><CR></blockquote><Esc>kkv0ojj$=
  nnoremap <leader>bq <Esc>i <blockquote class="quoteblock"><CR><++><CR></blockquote><Esc>kkv0ojj$=
  normal zM
endfunction

command! HtmlSyntax call HtmlSyntax()

function! NewHtmlSection()
  let id = input('Enter section-id (h2):', '')
  put='<section id=\"' . id . '\" rel=\"schema:hasPart\" resource=\"#'. id .'\">'
  put='  <h2 property=\"schema:name\"><++></h2>'
  put='  <div datatype=\"rdf:HTML\"  property=\"schema:description\" resource=\"#<++>\" typeof=\"<++>\">'
  put='    <++>'
  put='  </div><!--end div-h2-section-->'
  put='</section><!--end h2-section (' .id. ')-->'
  "replace whitespace before id (why does this happen?)
  ":%s/id=" /id="/
  ":%s/resource="# /resource="#/
  ":%s/section ( /section (/
  :normal kkkkkv0ojjjjj$=
  :call IMAP_Jumpfunc('', 0)
endfunction

function! NewHtmlSubSection()
  let id = input('Enter subsection-id (h3):', '')
  put='<section id=\"' . id . '\" rel=\"schema:hasPart\" resource=\"#'. id .'\">'
  put='  <h3 property=\"schema:name\"><++></h3>'
  put='  <div datatype=\"rdf:HTML\"  property=\"schema:description\" resource=\"#<++>\" typeof=\"<++>\">'
  put='    <++>'
  put='  </div><!--end div-h3-section-->'
  put='</section><!--end h3-section (' .id. ')-->'
  "replace whitespace before id (why does this happen?)
  ":%s/id=" /id="/
  ":%s/resource="# /resource="#/
  ":%s/section ( /section (/
  :normal kkkkkv0ojjjjj$=
  :call IMAP_Jumpfunc('', 0)
endfunction

function! NewHtmlSubSubSection()
  let id = input('Enter subsubsection-id (h4):', '')
  put='<section id=\"' . id . '\" rel=\"schema:hasPart\" resource=\"#'. id .'\">'
  put='  <h4 property=\"schema:name\"><++></h4>'
  put='  <div datatype=\"rdf:HTML\"  property=\"schema:description\" resource=\"#<++>\" typeof=\"<++>\">'
  put='    <++>'
  put='  </div><!--end div-h4-section-->'
  put='</section><!--end h4-section (' .id. ')-->'
  "replace whitespace before id (why does this happen?)
  ":%s/id=" /id="/
  ":%s/resource="# /resource="#/
  ":%s/section ( /section (/
  :normal kkkkkv0ojjjjj$=
  :call IMAP_Jumpfunc('', 0)
endfunction

function! NewHtmlSubSubSubSection()
  let id = input('Enter subsubsubsection-id (h5):', '')
  put='<section id=\"' . id . '\" rel=\"schema:hasPart\" resource=\"#'. id .'\">'
  put='  <h5 property=\"schema:name\"><++></h5>'
  put='  <div datatype=\"rdf:HTML\"  property=\"schema:description\" resource=\"#<++>\" typeof=\"<++>\">'
  put='    <++>'
  put='  </div><!--end div-h5-section-->'
  put='</section><!--end h5-section (' .id. ')-->'
  "replace whitespace before id (why does this happen?)
  ":%s/id=" /id="/
  ":%s/resource="# /resource="#/
  ":%s/section ( /section (/
  :normal kkkkkv0ojjjjj$=
  :call IMAP_Jumpfunc('', 0)
endfunction

function! CreateSection()
  let hnr = input('h-nr:')
  if hnr  == 2
    :call NewHtmlSection()
  elseif hnr == 3
    :call NewHtmlSubSection()
  elseif hnr == 4
    :call NewHtmlSubSubSection()
  elseif hnr == 5
    :call NewHtmlSubSubSubSection()
  endif
endfunction

function! UpdateHtmlColors()
  hi Special ctermfg=80
  hi htmlLink ctermfg=75
  " default: term=italic cterm=italic gui=italic
  hi htmlItalic term=italic ctermbg=238 ctermfg=255 gui=bold
  hi htmlBold term=bold cterm=bold gui=bold
  hi Special term=bold cterm=bold gui=bold
endfunction

function! UpdateMdColors()
  hi Special ctermfg=80
  "hi htmlLink ctermfg=39
  hi htmlLink ctermfg=lightblue
  hi htmlString ctermfg=37
  hi String ctermfg=108
  hi htmlBold term=bold cterm=bold gui=bold
  hi htmlItalic term=italic ctermbg=238 ctermfg=255 gui=bold
  hi clear SpellBad |
  "set noautoindent |
  hi SpellBad cterm=underline ctermfg=red  |
  match ErrorMsg '\s\+$' |
  hi ErrorMsg ctermbg=242
endfunction

function! RemoveTrimWhitespace()
  command! TrimWhitespace echo "No man, not in here"
endfunction
