" SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
" SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
"
" SPDX-License-Identifier: CC0-1.0

"#########################################
"###############  GENERAL  ###############
"#########################################

"-----------------------------------------------------
" Default configs and themes are in: /usr/share/vim/
"-----------------------------------------------------

"map the leader key (3 options)
let mapleader = '\'
nmap , \
nmap <space> \

"Set lineNumbering:
set nu
"no linebreaking
set linebreak
set tw=79

set t_Co=256
set nocompatible
"filetype off
set encoding=utf-8

"To insert space characters whenever the tab key is pressed:
set expandtab

"set autoindent

syntax on

set tabstop=2
set softtabstop=2
set shiftwidth=2

"Allow opening files form vim explores with "x"
let g:netrw_browsex_viewer= "xdg-open"
let g:netrw_localmovecmd="mv"
let g:netrw_localcopycmd="cp"

"Don't show swp files in explorer:
set directory=$HOME/.vim/swapfiles/
" Hide pyc files
let g:netrw_list_hide= '*\.pyc$'
let g:netrw_list_hide= '*\.pyc'
let g:netrw_list_hide= '*.pyc'

"MAP SAVING
noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>

"Check out and open recenly opened files
nmap <leader>o  :browse oldfiles<CR>

"copy to clipboard vi shell
vmap <leader>c :w !pbcopy <CR><CR>
nmap <leader>C :%w !pbcopy <CR><CR>

"some quick and dirty copy-paste work and textwidth
nmap <leader>va ggvG$
vmap a g<Esc>ggvG
nmap <leader>gq gggqG
nmap <C-a> ggvG$
vmap <C-a> <Esc>ggvG$
imap <C-a> <Esc>ggvG$

"set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" Add all your plugins here (note older versions of Vundle used Bundle
" instead of Plugin)
" General:
Plugin 'vim-airline/vim-airline'
Plugin 'nvie/vim-flake8'
Plugin 'tmhedberg/simpylfold'
Plugin 'hdima/python-syntax'
Plugin 'marcweber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'jnurmine/zenburn'
Plugin 'altercation/vim-colors-solarized'
Plugin 'chriskempson/vim-tomorrow-theme'
Plugin 'scrooloose/nerdcommenter'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'wojtekmach/vim-rename'
Plugin 'NLKNguyen/papercolor-theme'
Plugin 'therubymug/vim-pyte'
Plugin 'gerw/vim-latex-suite'
Plugin 'w0rp/ale'
Plugin 'protesilaos/tempus-themes-vim'
Plugin 'godlygeek/tabular'
Plugin 'ervandew/supertab'
Plugin 'pangloss/vim-javascript'
Plugin 'tpope/vim-surround'
"Plugin 'asciidoc/vim-asciidoc'
Plugin 'habamax/vim-asciidoctor'
Plugin 'plasticboy/vim-markdown'
Plugin 'leafgarland/typescript-vim'
Plugin 'SirVer/ultisnips'
" All of your Plugins must be added before the following line
"Plugin 'vim-scripts/indentpython.vim'
" Not used (anymore):
"Plugin 'morhetz/gruvbox'z
"Plugin 'vimwiki/vimwiki'
"Plugin 'kien/ctrlp.vim'
"Plugin 'farseer90718/vim-taskwarrior'
"Plugin 'Shougo/vimproc.vim'
"Plugin 'Shougo/unite.vim'
"Plugin 'scrooloose/syntastic' "use Ale

call vundle#end()            " required

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim', {'commit': '4145f53f3d343c389ff974b1f1a68eeb39fba18b'}
Plug 'jonsmithers/vim-html-template-literals'
Plug 'pangloss/vim-javascript'
" Initialize plugin system
call plug#end()

filetype plugin indent on    " required


" SNIPPETS snippets
"
" edit/modify/add snippets
" ~/.vim/bundle/vim-snippets/snippets
"
" https://github.com/SirVer/ultisnips

" set snipmate version (0 or 1)
let g:snipMate = { 'snippet_version' : 1 }

"" - Avoid using standard Vim directory names like 'plugin'
"call plug#begin('~/.vim/plugged')

"" Make sure you use single quotes
"Plug 'calviken/vim-gdscript3'
"" Initialize plugin system
"call plug#end()

" vim autocmd
source ~/.vim/svbaelen/vim_ftype_autocmd

"Installed fuzzy finder through git
"https://vimawesome.com/plugin/fzf#installation
set rtp+=~/.fzf
nnoremap <leader><space> :FZF! ~/ <cr>
nnoremap <C-t> :FZF!~/ <cr>>
nnoremap <leader>a :FZF /  <cr>
"find in buffers
nnoremap <leader>f :Lines <cr>
"find in this buffer
nnoremap <leader>bf :BLines <cr>
nnoremap <leader>h :History  <cr>

"Navigate between windows
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

"Vim-Airline already has a function to show the buffers:
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" This allows buffers to be hidden if you've modified a buffer.
" This is almost a must if you wish to use buffers in this way.
set hidden

" To open a new empty buffer
"nmap <qleader>e :enew<cr>
"nmap <leader>be :enew<cr>

"nmap <leader>E :e ~/Documents/personal/software/latex/temp_doc <cr>
nmap <leader>bt :e ~/Documents/personal/software/latex/temp_doc <cr>

" Delete buffer
nmap <leader>bd :bd <cr>

" Move to the next buffer
"nmap on :bnext<CR>
nmap <S-b> :bnext<CR>

" Move to the previous buffer
"nmap op :bprevious<CR>
nmap <S-p> :bprevious<CR>
nmap <leader>p :bprevious<CR>

" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>
nmap <leader>bc :bp <BAR> bd #<CR>

" Show all open buffers and their status (fzf plugin)
nmap <leader>bb :Buffer<CR>
nmap <leader>bl :Buffer<CR>

set splitbelow
set splitright

"split navigations
nnoremap  <A-J> <C-W><C-J>
nnoremap <A-K> <C-W><C-K>
nnoremap <A-L> <C-W><C-L>
nnoremap <A-H> <C-W><C-H>
nnoremap  <A-Up> <C-W><C-J>
nnoremap <A-Down> <C-W><C-K>
nnoremap <A-Right> <C-W><C-L>
nnoremap <A-Left> <C-W><C-H>

"highligh
set hlsearch
"toggle between autowrap and wrap when formatting
"http://blog.ezyang.com/2010/03/vim-textwidth/
set fo+=t "autowrap by default
autocmd BufNewFile,BufRead * setlocal fo+=t
let s:wrap = 0
function! ToggleAutowrap()
  if s:wrap  == 0
    let s:wrap = 1
    setlocal fo-=t
  else
    let s:wrap = 0
    setlocal fo+=t
  endif
endfunction

nnoremap <leader>w :call ToggleAutowrap()<CR>

"augroup vimrc_autocmds
"autocmd BufEnter * highlight OverLength ctermbg=darkgrey guibg=#592929
"autocmd BufEnter * match OverLength /\%80v.*/
"augroup END

"toggle colorhighlight when > tw=76
let s:color = 1
function! TextwidthColorToggle()
  if expand('%:p:h') != "/tmp/mutt"
    if s:color  == 0
      let s:color = 1
      highlight OverLength ctermbg=darkgrey guibg=#592929
      match OverLength /\%1000000v.*/
    else
      let s:color = 0
      highlight OverLength ctermbg=darkgrey guibg=#592929
      match OverLength /\%79v.*/
    endif
  endif
endfunction
command! ColorTextwidthToggle call TextwidthColorToggle()

" Enable/Disable ColorToggle at launch
"au BufNewFile,BufRead * call TextwidthColorToggle()

"cancel highlight
inoremap <C-f> <Esc> :nohlsearch <CR><i>
nnoremap <C-f> :nohlsearch <CR>

"Delete spacings
vnoremap <C-x>  :s/ //g  <CR> <ESC> :nohlsearch <CR>


"Vimsplit links
"au CursorMovedI *.md call ModifyTextWidth() " Use only within *.md files

function! ModifyTextWidth()
  if getline(".")=~'^.*\[.*\](.*)$' " If the line ends with Markdown link -
    set big value for textwidth
    setlocal textwidth=500
  else
    setlocal textwidth=80 "Otherwise use normal textwidth
  endif
endfunction

function! GetModSlate()
  hi Comment term=bold ctermfg=242 guifg=grey40
  "hi String term=bold guifg=SkyBlue ctermfg=cyan
  "hi String term=bold guifg=SkyBlue ctermfg=106
  hi Identifier guifg=salmon ctermfg=114
  hi Include guifg=red ctermfg=114
  hi PreProc guifg=red guibg=white ctermfg=114
  hi Operator guifg=Red ctermfg=114
  hi search guibg=peru guifg=wheat cterm=none ctermfg=black ctermbg=yellow
  hi clear SpellBad
  hi clear SpellCap
  hi clear SpellLocal
  "hi clear SpellRare
  hi SpellBad cterm=underline ctermfg=196
  hi SpellLocal ctermfg=237 ctermbg=253
  "hi SpellRare ctermfg=243 ctermbg=255
  "hi SpellErrors guifg=White guibg=Red cterm=underline ctermfg=7
  "hi LineNr guifg=grey50 ctermfg=114
  hi LineNr guifg=grey50 ctermfg=242
  "hi Function guifg=navajowhite ctermfg=114
  "hi Type guifg=CornflowerBlue ctermfg=2
  hi Constant guifg=#ffa0a0 ctermfg=28 "brown is standard
  "hi Special guifg=darkkhaki ctermfg=131
  hi Special guifg=darkkhaki ctermfg=101
  hi Visual  cterm=reverse ctermbg=black

endfunction

" Mark trailing spaces, so we know we are doing flowed format right
" \ match ErrorMsg '\s\+$' |

"NERDTree when starting vim. use :NERDTree for this
"--> https://github.com/jistr/vim-nerdtree-tabs
let g:nerdtree_tabs_open_on_gui_startup=0
let g:nerdtree_tabs_open_on_console_startup=0
"let g:nerdtree_tabs_open_on_new_tab=0
"

" Markdown markdown
" Enable folding
"set foldmethod=indent
"set foldlevel=99
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_no_extensions_in_markdown = 1
set conceallevel=2
" disable concealing
"let g:vim_markdown_conceal = 0
" no concealing in latex math model
let g:tex_conceal = ""
let g:vim_markdown_math = 1
" no concealing in code blocks
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_fenced_languages = ['csharp=cs']

" shortcut function to enable folding
fun! MdSetFold()
  let g:vim_markdown_folding_disabled = 0
  execute 'edit'
  execute ':normal G'
  execute ':normal zO'
  execute ':normal gg'
endfun
command! MdFold call MdSetFold()
command! MDFold call MdSetFold()
command! MdSetFold call MdSetFold()
command! MDSetFold call MdSetFold()

"let g:SimpylFold_docstring_preview=1

"For 'YouCompleteMe'
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
let g:ycm_key_list_select_completion = ['<Down>']
let g:ycm_key_list_previous_completion = ['<Up>']

"Some general completion pop up changes (otherwise YCM interfered with
"SnipMate):


if has('gui_running') "because in terminal this complicates the arrow keys
  functions
  inoremap <expr> <Esc>      pumvisible() ? "\<C-e>" : "\<Esc>"
endif

inoremap <expr> <CR>       pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <expr> <Down>     pumvisible() ? "\<C-n>" : "\<Down>"
inoremap <expr> <Up>       pumvisible() ? "\<C-p>" : "\<Up>"
inoremap <expr> <PageDown> pumvisible() ? "\<PageDown>\<C-p>\<C-n>" :
"\<PageDown>"
inoremap <expr> <PageUp>   pumvisible() ? "\<PageUp>\<C-p>\<C-n>" :
"\<PageUp>"

"Vim usually has its own clipboard and ignores the system keyboards, but
"sometimes you might want to cut,
"copy, and/or paste to/from other applications outside of VIM.
"On OSX, you can access your system clipboard with this line:
set clipboard=unnamed

"You still have to go to the end of the file
"otherwise <D-y> will have another (edit) function
nnoremap <D-y> :let g:ycm_auto_trigger=0<CR>                "turn off YCM
nnoremap <D-Y> :let g:ycm_auto_trigger=1<CR>                "turn on YCM

fun! TrimWhitespace()
  let l:save_cursor = getpos('.')
  %s/\s\+$//e
  call setpos('.', l:save_cursor)
endfun
command! TrimWhitespace call TrimWhitespace()

"hide/show status bar (or line)
set laststatus=0

let s:hidden_all = 0
function! ToggleHiddenAll()
  if s:hidden_all  == 0
    let s:hidden_all = 1
    "set noshowmode
    "set noruler
    set laststatus=0
    "set noshowcmd
  else
    let s:hidden_all = 0
    set showmode
    set ruler
    set laststatus=2
    set showcmd
  endif
endfunction

"Toggle Status Bar bottom
nnoremap <leader>b :call ToggleHiddenAll()<CR>

let g:airline_theme='luna'

"=========================================================
"Coloschemes
"=========================================================
"---------------------------------------------------------
" Customized colorschemes from plugins
"---------------------------------------------------------"

" CUSTOMIZE COLORSCHEME (needs to be _before_ PaperColor command
let g:PaperColor_Theme_Options = {
      \   'theme': {
      \     'default.light': {
      \				'transparent_background': 1,
      \       'override' : {
      \         'color00' : ['#FFFFFF', '231'],
      \         'linenumber_fg' : ['#808080', '8'],
      \         'linenumber_bg' : ['#FFFFFF', '230'],
      \         'color07' : ['#000000', '232']
      \       }
      \     }
      \   }
      \ }

"---------------------------------------------------------
" Set color schemes
"---------------------------------------------------------"


" check that we are _inside_ a tmux session
let tmux = $TMUX

if strchars(tmux) > 0
  " use list to avoid enter input
  let current_scheme = systemlist("tmux show-environment -g TMUX_COLORSCHEME | sed 's:^.*=::'")[0]
  if current_scheme == "black"
    set background=dark
    colorscheme ron
  elseif current_scheme == "white"
    set background=light
    colorscheme PaperColor
  endif
else
  set background=dark
  colorscheme ron
endif

"if has("gui_running")
""Just open zenburn
"colorscheme zenburn
"AirlineTheme dark
"else
"colorscheme default
""AirlineTheme molokai
"set background=dark
"endif


function! ColorScheme()
  let scheme = input('Pick a ColorScheme: [1:Default],[2:Sol-Dark],[3:Sol-Light],[4:Zenburn],[5:Tomorrow Dark],[6:Tomorrow Light],[7:Tomorrow 80s],[8:Tomorrow Night Bright], [9: gruvbox], [10: papercolor], [11: slate], [12: slate modified]: ')
  if scheme==1
    "Terminal or GUI
    set background=dark
    colorscheme ron
    AirlineTheme luna
  elseif scheme==2
    set background=dark
    colorscheme solarized
    AirlineTheme luna
  elseif scheme==3
    set background=light
    colorscheme solarized
    AirlineTheme luna
  elseif scheme==4
    set background=dark
    colorscheme zenburn
    AirlineTheme luna
  elseif scheme==5
    colorscheme Tomorrow-Night
    AirlineTheme luna
  elseif scheme==6
    colorscheme Tomorrow
    AirlineTheme luna
  elseif scheme==7
    colorscheme Tomorrow-Night-Eighties
    AirlineTheme luna
  elseif scheme==8
    colorscheme Tomorrow-Night-Bright
    AirlineTheme luna
  elseif scheme==9
    set background=dark
    colorscheme gruvbox
    AirlineTheme luna
  elseif scheme==10
    set background=light
    colorscheme PaperColor
    AirlineTheme luna
  elseif scheme==11
    set background=dark
    colorscheme slate
    AirlineTheme luna
  elseif scheme==12
    set background=dark
    colorscheme slate
    AirlineTheme luna
    call GetModSlate()
  endif
endfunction

"Or just call function after setup
command! ColorScheme call ColorScheme()

map <F5> :ColorScheme <cr>

" To get more contrast to the Visual selection, use
"let g:zenburn_alternate_Visual = 1
"   Note: this is enabled only if the old-style Visual
"   if used, see g:zenburn_old_Visual
"let g:zenburn_old_Visual = 1
"To use alternate colouring for Error message, use
"let g:zenburn_alternate_Error = 0
"
" * The new default for Include is a duller orange. To use the original
"   colouring for Include, use
"
"let g:zenburn_alternate_Include = 1

" set file for auto-downloading spell files
let g:spellfile_URL = 'http://ftp.vim.org/vim/runtime/spell'

function! Spelling()
  let spelling = input('Pick a spelling language: [0:None], [1:English-US], [2:English-GB], [3:Nederlands]: ')
  if spelling==0
    set nospell
  elseif spelling==1
    set spell spelllang=en_us
  elseif spelling==2
    set spell spelllang=en_gb
  elseif spelling==3
    set spell spelllang=nl
  endif
  " Spelling not for URLs
  syn match UrlNoSpell '\w\+:\/\/[^[:space:]]\+' contains=@NoSpell
endfunction

command! Spelling call Spelling()
"set spell spelllang=en_gb

"Airline (bar)
let g:airline_mode_map = {
      \ '__' : '-----',
      \ 'n'  : '    NORMAL    ',
      \ 'i'  : '    INSERT    ',
      \ 'v'  : '    VISUAL    ',
      \ 'V'  : '    VISUAL    ',
      \ }

"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#show_close_button = 1
"let g:airline#extensions#tabline#switch_buffers_and_tabs = 1
"let g:airline#extensions#tabline#fnamemod = ':t'

" /usr/share/vim/vim80/colors/
" Find variables you want to modify and check out the the default vim
" syntax, i.e. in /usr/share/vim/vim80/syntax/<language>.vim
"
" It is best to re-define some variables in your vimrc and leave the
" original
" files as-is.
"
" Check your current buffer syntax: ':setlocal syntax?'
"

"########################################
"####### General macros for vim   #######
"########################################
"
source ~/.vim/svbaelen/vim_macros

"########################################
"########  Formatting functions   #######
"########################################

source ~/.vim/svbaelen/vim_text_format

"########################################
"#######   NERDTREE COMMENTER    ########
"########################################
let g:NERDCustomDelimiters = { 'c': { 'left': '//','right': '' } }
let g:NERDCustomDelimiters = { 'cpp': { 'left': '//','right': '' } }
let g:NERDCustomDelimiters = { 'moos': { 'left': '//','right': '' } }
let g:NERDCustomDelimiters = { 'h': { 'left': '//','right': '' } }
let g:NERDCustomDelimiters = { 'bhv': { 'left': '//','right': '' } }
let g:NERDCustomDelimiters = { 'tex': { 'left': '%','right': '' } }
"NERDTree comments for .map files
let g:NERDCustomDelimiters = { 'map': { 'left': '#','right': '' } }


"########################################
"############### VIM WIKI ###############
"#######################################
"let g:vimwiki_list = [{'path': '$HOME/GoogleDrive/SenneVanBaelen/wiki/'}]
"insert new file
"nmap <leader>wn :r new.vim <CR>

"#########################################
"###############  PYTHON  ################
"#########################################

let python_highlight_space_errors = 0
let python_highlight_all=1
let NERDTreeIgnore = ['\.pyc$']

"########################################
"################  HTML   ################
"#########################################

"Custom mappings surround plugin
vmap Sc S<code>
vmap Sp S<p>
vmap Sa S<a href="">v$:s/href=""/href="<++>"/ <CR><C-j>
vmap Sb S<b>
vmap Si S<i>
vmap Ss S<samp>
vmap Sl S<li>
" wrap every line of selection with <samp></samp>
vmap SS :normal yss<samp><CR>
" wrap every line of selection with <li></li>
vmap SL :normal yss<li><CR>

"https://github.com/tpope/vim-surround

function! RunConcatHtml()
  " this function prints all info messages, and put the bibtex warnings in
  " an alert
  ":!./utils/concat_html.sh -r -ro "-lf -http -bib -s ieeetr"
  :!./utils/concat_html.sh -i "./public/html" -m "public/_main.html" -o "public" -r -ro "-lf -http -bib -s ieeetr"
  :!./utils/refresh_firefox_tab.sh --tab-nr 1
endfunction
function! RunConcatHtmlSwitch()
  " this functions runs quietly (no warnings in alert)
  :!./utils/concat_html.sh -i "./public/html" -m "public/_main.html" -o "public"  -r -q -ro "-q -lf -http -bib -s ieeetr"
  :!./utils/refresh_firefox_tab.sh --switch-window --tab-nr 1
endfunction

"Lots of macro's defined in ~/.vim/svbaelen/vim_ftype_autocmd"

" next line is added to ~/.vim/syntax/html.vim (and full
" /usr/share/vim/vim82/syntax/html.vim is copied)
"syn region htmlCode start="<code\>" end="</code\_s*>"me=s-1 contains=@htmlTop,@NoSpell

"guibg=peru guifg=wheat ctermfg=black ctermbg=yellow
"
" https://github.com/jonsmithers/vim-html-template-literals#supported-syntaxes-inside-html-
let g:html_indent_style1 = "inc"

"#########################################
"################  LATEX  ################
"#########################################

set grepprg=grep\ -nH\ $*
let g:tex_comment_nospell=1

"FOR SAVING MACROS: shortcut
cmap w!! %!sudo tee > /dev/null

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults
" to 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':

let g:tex_flavor='latex'
let g:Tex_TreatMacViewerAsUNIX = 1
let g:Tex_ExecuteUNIXViewerInForeground = 1
let g:Tex_ViewRule_ps = 'xdg-open'
let g:Tex_ViewRule_pdf = 'xdg-open'
let g:Tex_ViewRule_dvi = 'xdg-open'
"let g:Tex_FormatDependency_dvi = 'dvi,ps,pdf'"
let g:Tex_DefaultTargetFormat='pdf'

"Don't open files if error automatically
let g:Tex_GotoError=0
let g:Tex_MultipleCompileFormats='pdf,bib,dvi,pdf'
"let g:Tex_MakeIndexFlavor = 'makeindex $*.idx, makeindex $*.nlo, makeindex
"$*.nls'

"nmap <Leader>f :call MakeTexFolds(1) <CR>

""TRY TO USE W/ BIBER (it works, but rather cumbersome)
""https://tex.stackexchange.com/questions/83715/biber-backend-and-vim-latex
"let g:Tex_BibtexFlavor = 'biber'

"map <Leader>lb :<C-U>exec '!biber '.Tex_GetMainFileName(':p:t:r')<CR>


"ASCII DOCTOR

" Function to create buffer local mappings and add default compiler
fun! AsciidoctorMappings()
    nnoremap <buffer> <leader>oo :AsciidoctorOpenRAW<CR>
    nnoremap <buffer> <leader>op :AsciidoctorOpenPDF<CR>
    nnoremap <buffer> <leader>oh :AsciidoctorOpenHTML<CR>
    nnoremap <buffer> <leader>ox :AsciidoctorOpenDOCX<CR>
    nnoremap <buffer> <leader>ch :Asciidoctor2HTML<CR>
    nnoremap <buffer> <leader>cp :Asciidoctor2PDF<CR>
    nnoremap <buffer> <leader>cx :Asciidoctor2DOCX<CR>
    nnoremap <buffer> <leader>p :AsciidoctorPasteImage<CR>
    " :make will build pdfs
    compiler asciidoctor2pdf
endfun

" Call AsciidoctorMappings for all `*.adoc` and `*.asciidoc` files
augroup asciidoctor
    au!
    au BufEnter *.adoc,*.asciidoc call AsciidoctorMappings()
augroup END


"#########################################
"################   ALE   ################
"#########################################

"Check current enabled linters and configuration (variables):
" :ALEInfo
" (!! different for every filetype, obviously)

nnoremap <leader>t :ALEToggle <CR>

" Note: this requires the user to have the linters (e.g. flake8, pylint,
" ...) instlalled on the user's system.
"
" $ which flake8 -> /usr/bin/flake8
" When pylint is installed (pip package):
" -> $ which pylint -> /home/svbaelen/.local/bin/pylint
"
" PYLINT CONFIG:
" -> ~/.pylintrc"
"

" other (locally) availble latex options:
" alex.vim (unset here, because the check for "profanities" can be a bit too
" low-treshold in my opinion

"let g:ale_python_flake8_auto_pipenv = 0
"let g:ale_python_flake8_change_directory = 1
"let g:ale_python_flake8_executable = 'flake8'
"let g:ale_python_flake8_options = ''
"let g:ale_python_flake8_use_global = 0
"let g:ale_python_mypy_auto_pipenv = 0
"let g:ale_python_mypy_executable = 'mypy'
"let g:ale_python_mypy_ignore_invalid_syntax = 0
"let g:ale_python_mypy_options = ''
"let g:ale_python_mypy_use_global = 0
"let g:ale_python_pylint_auto_pipenv = 0
"let g:ale_python_pylint_change_directory = 1
"let g:ale_python_pylint_executable = 'pylint' let
"g:ale_python_pylint_options = ''
"let g:ale_python_pylint_use_global = 0
"
"let g:ale_python_pylint_executable = ''


"---------------------------------------------------------------------
"C/CPP
"---------------------------------------------------------------------
":help ale-cpp-options

let g:ale_c_parse_makefile=1
let g:ale_c_gcc_options = '-std=gnu11 -Wall -I/usr/include -I/usr/local/include -I../include -I../../include -I/home/svbaelen/Documents/work/kuleuven/iwt/communication/kafka/ssa-client-c/include -I/home/svbaelen/Documents/personal/software/graphics-engines/bgfx/tutorials/svbaelen-basics/thirdparty/glfw/include -I/home/svbaelen/Documents/personal/software/graphics-engines/bgfx/tutorials/svbaelen-basics/thirdparty/bgfx/bgfx/include -I/home/svbaelen/Documents/personal/software/graphics-engines/bgfx/tutorials/svbaelen-basics/thirdparty/bgfx/bx/include -I/home/svbaelen/Documents/personal/software/graphics-engines/bgfx/tutorials/svbaelen-basics/thirdparty/bgfx/bimg/include'

" I currently use gcc as 'compiler option', so only the gcc linter is
" relevant
let g:ale_cpp_cpplint_options=1
"FOR MOOS (TODO: detect automatically)
"Make sure gcc can detect all the headers
let g:ale_cpp_cc_options = '-std=c++17 -Wall -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/include/ivp -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/App/include -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/Comms/include  -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/DB/include -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/include  -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/testing/include  -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/Thirdparty/include -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/Utils/include -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/Thirdparty/getpot/include -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/Thirdparty/PocoBits/include  -I/home/svbaelen/Documents/work/kuleuven/iwt/moos-imp/MOOS_Jul0519/MOOSCore/Core/libMOOS/Thirdparty/AppCasting/include -I/home/svbaelen/Documents/personal/software/graphics-engines/tutorials/svbaelen-basics/thirdparty/bgfx/bgfx/include -I/home/svbaelen/Documents/personal/software/graphics-engines/tutorials/svbaelen-basics/thirdparty/bgfx/bx/include -I/home/svbaelen/Documents/personal/software/graphics-engines/tutorials/svbaelen-basics/thirdparty/bgfx/bimg/include -I/home/svbaelen/Documents/personal/software/graphics-engines/tutorials/svbaelen-basics/thirdparty/glfw/include -I/home/svbaelen/Documents/personal/software/graphics-engines/tutorials/svbaelen-basics/thirdparty/bgfx/bgfx/examples/common'

"CPP options for compiler
"https://github.com/dense-analysis/ale#5xv-how-can-i-configure-my-c-or-c-project

" Current Filetype: cpp
" Available linters: ['ccls', 'clang', 'clangcheck', 'clangd', 'clangtidy',
"                     'clazy', 'cppcheck', 'cpplint', 'cquery',
"                     'flawfinder', 'gcc']
"
" HTML available: ['alex', 'fecs', 'htmlhint', 'proselint', 'stylelint',
"                  'tidy', 'writegood'],
" Disable alex (catches insensitive, inconsiderate writing)
"<github.com/get-alex/alex>


"Javscript
" ESLint configured by default
" Best to add rule to local .eslintrc.{yml,json,...} to make unused
" variables a warning instead of error:
" <https://stackoverflow.com/a/59225562>

" Disable quote concealing in JSON files
let g:vim_json_conceal=0

" ALE styling
"let g:ale_sign_error = '✘'
"let g:ale_sign_warning = '⚠'
"highlight ALEErrorSign ctermbg=NONE ctermfg=red
"highlight ALEWarningSign ctermbg=NONE ctermfg=yellow
"
"help ale-java-options
let g:ale_java_javac_classpath = '/home/svbaelen/Documents/work/kuleuven/iwt/data-models/apache-jena/apache-jena-3.16.0/lib/*'

" SET ALE LINTERS:
let g:ale_linters = {
\   'cpp':['gcc','clangtidy','clang','clangcheck','cpplint','cppcheck',
\          'ccls','clangd','clangcheck','claze'],
\   'html':['fecs', 'htmlhint', 'proselint', 'stylelint', 'tidy',
\           'writegood'],
\   'tex':['chktex'],
\}

let g:ale_linters_ignore = ['flake8']

let g:ale_fixers = {}
let g:ale_fixers.python = ['black']
let g:ale_python_auto_pipenv = 1


" Disable auto-detection of virtualenvironments
let g:ale_virtualenv_dir_names = []
" Environment variable ${VIRTUAL_ENV} is always used


"=====================================================================
"Path
"====================================================================="
"local includes
set path+=/usr/local/include
